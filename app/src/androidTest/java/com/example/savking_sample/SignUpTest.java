package com.example.savking_sample;

import android.content.Context;
import android.content.res.Resources;
import android.os.SystemClock;

import androidx.test.InstrumentationRegistry;
import androidx.test.ext.junit.rules.ActivityScenarioRule;

import com.example.savking_sample.signUp.SignUpActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class SignUpTest {
    public String pseudo ="tttt";
    public String password="tttt";
    public String email="tttt";

    @Rule
    public ActivityScenarioRule<SignUpActivity> activityRule
            = new ActivityScenarioRule<>(SignUpActivity.class);

    @Before
    public void initValidString() {
        //Resources resources = InstrumentationRegistry.getTargetContext().getResources();

    }

    @Test
    public void signUp() {

        ActionDuringTest.clickOnIt(R.id.signup_pseudo_edt);
        ActionDuringTest.typeSomething(R.id.signup_pseudo_edt, pseudo);
        ActionDuringTest.clickOnIt(R.id.signup_email_edt);
        ActionDuringTest.typeSomething(R.id.signup_email_edt, email);
        ActionDuringTest.typeSomething(R.id.signup_password_edt, password);
        ActionDuringTest.typeSomething(R.id.signup_password_second_edt, password);
        ActionDuringTest.clickOnIt(R.id.signup_create);
        SystemClock.sleep(2000);

    }
}
