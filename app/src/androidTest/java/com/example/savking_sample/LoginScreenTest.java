package com.example.savking_sample;

import android.content.Context;
import android.content.res.Resources;
import android.os.SystemClock;

import androidx.test.InstrumentationRegistry;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class LoginScreenTest {


    private String messageEmptyPseudo;
    private String messageEmptyPassword;


    @Rule
    public ActivityScenarioRule<LoginActivity> activityRule
            = new ActivityScenarioRule<>(LoginActivity.class);

    @Before
    public void initValidString() {
        Resources resources = InstrumentationRegistry.getTargetContext().getResources();
        messageEmptyPseudo = resources.getString(R.string.toast_empty_pseudo);
        messageEmptyPassword = resources.getString(R.string.toast_empty_password);
        //  String.valueOf(R.string.toast_empty_pseudo);
    }

    @Test
    public void typingNothingAndConnect() {

        ActionDuringTest.clickOnIt(R.id.login_connexion_btn);
        Context appContext = androidx.test.platform.app.InstrumentationRegistry.getInstrumentation().getTargetContext();
        ActionDuringTest.scrollAndClick(R.id.login_connexion_btn);

        ActionDuringTest.toastChecking(messageEmptyPseudo);
    }


    @Test
    public void typingOnlyPseudoAndConnect() {
        ActionDuringTest.typeSomething(R.id.login_edt_pseudo, "pseudo");
        ActionDuringTest.scrollAndClick(R.id.login_connexion_btn);
        SystemClock.sleep(3000);
        ActionDuringTest.toastChecking( messageEmptyPassword);
    }

    @Test
    public void typingOnlyPasswordAndConnect() {

        ActionDuringTest.typeSomething(R.id.login_edt_password, "Something");

        ActionDuringTest.scrollAndClick(R.id.login_connexion_btn);

        ActionDuringTest.toastChecking(messageEmptyPseudo);

    }

    @Test
    public void checkingLogin() {
        ActionDuringTest.typeSomething(R.id.login_edt_pseudo, "ttt");
        ActionDuringTest.typeSomething(R.id.login_edt_password, "ttt");
        ActionDuringTest.scrollAndClick(R.id.login_connexion_btn);
        SystemClock.sleep(3000);
    }

    @Test
    public void goToSignUp() {
        ActionDuringTest.scrollAndClick(R.id.login_btn_new_user);
        SystemClock.sleep(1000);
    }

}
