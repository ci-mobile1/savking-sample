package com.example.savking_sample;

import android.content.Context;
import android.content.res.Resources;
import android.os.SystemClock;

import androidx.test.InstrumentationRegistry;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainScreenTest {


    private String title;
    private String description;


    @Rule
    public ActivityScenarioRule<MainActivity> activityRule
            = new ActivityScenarioRule<>(MainActivity.class);

    @Before
    public void initValidString() {
        Resources resources = InstrumentationRegistry.getTargetContext().getResources();
        title = resources.getString(R.string.toast_empty_pseudo);
        description = resources.getString(R.string.toast_empty_password);
        //  String.valueOf(R.string.toast_empty_pseudo);
    }

    @Test
    public void MaiNavigationTest() {
        ActionDuringTest.clickOnIt(R.id.navigation_cashboard);
        SystemClock.sleep(3000);
        ActionDuringTest.clickOnIt(R.id.navigation_profil);
        SystemClock.sleep(3000);
        ActionDuringTest.clickOnIt(R.id.navigation_home);
        SystemClock.sleep(3000);

    }

}




