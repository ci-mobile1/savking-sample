package com.example.savking_sample;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class ActionDuringTest {


    public static void toastChecking(String wordShouldAppear ) {

        onView(withText(wordShouldAppear)).inRoot(new com.example.savking_sample.ToastMatcher())
                .check(matches(isDisplayed()));
        onView(withText(wordShouldAppear)).inRoot(new com.example.savking_sample.ToastMatcher())
                .check(matches(withText(wordShouldAppear)));
    }

    public static void typeSomething(int idComponent , String wordToTyped) {
        onView(withId(idComponent))
                .perform(typeText(wordToTyped), closeSoftKeyboard());
    }

    public static void scrollAndClick(int idComponent){
        onView(withId(idComponent)).perform(scrollTo(), click());
    }
    public static void clickOnIt(int idComponent){
        onView(withId(idComponent)).perform(click());
    }
    public static void checkTextTyped(String message) {
        onView(withId(R.id.login_edt_pseudo))
                .check(matches(withText(message)));
    }
}
