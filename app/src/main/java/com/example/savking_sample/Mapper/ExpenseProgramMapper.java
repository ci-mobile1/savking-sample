package com.example.savking_sample.Mapper;
import com.example.savking_sample.DTO.ExpenseProgramDTO;
import com.example.savking_sample.models.ExpenseProgramModel;

import java.util.ArrayList;
import java.util.List;

public class ExpenseProgramMapper {
    public static List<ExpenseProgramModel> map(List<ExpenseProgramDTO> expenseProgramDTOS) {
        List<ExpenseProgramModel> expenseProgramModels = new ArrayList<>();
        for (ExpenseProgramDTO expenseProgramDTO : expenseProgramDTOS) {
            expenseProgramModels.add(map(expenseProgramDTO));
        }
        return expenseProgramModels;
    }

    private static ExpenseProgramModel map(ExpenseProgramDTO expenseProgramDTO) {
        ExpenseProgramModel expenseProgramModel = new ExpenseProgramModel();
        expenseProgramModel.seteCategory(expenseProgramDTO.geteCategory());
        expenseProgramModel.seteCreditCard(expenseProgramDTO.geteCreditCard());
        expenseProgramModel.seteGreenCash(expenseProgramDTO.geteGreenCash());
        expenseProgramModel.seteTR(expenseProgramDTO.geteTR());
        expenseProgramModel.seteDateBegin(expenseProgramDTO.geteDateBegin());
        expenseProgramModel.seteLastTimeAdded(expenseProgramDTO.geteLastTimeAdded());
        expenseProgramModel.seteTypeOfProgram(expenseProgramDTO.geteTypeOfProgram());
        expenseProgramModel.seteIdUser(expenseProgramDTO.getId());
        expenseProgramModel.set_id(expenseProgramDTO.getId());
        expenseProgramModel.seteTitle(expenseProgramDTO.geteTitle());
        expenseProgramModel.seteDescription(expenseProgramDTO.geteDescription());
        expenseProgramModel.seteSwitch(expenseProgramDTO.iseSwitch());
        expenseProgramModel.seteDateEnd(expenseProgramDTO.geteDateEnd());
        return expenseProgramModel;
    }
}
