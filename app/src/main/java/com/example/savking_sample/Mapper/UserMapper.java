package com.example.savking_sample.Mapper;

import com.example.savking_sample.DTO.UserDTO;
import com.example.savking_sample.models.UserModel;

import java.util.ArrayList;
import java.util.List;

public class UserMapper {

    public static List<UserModel> map(List<UserDTO> userDTOList) {
        List<UserModel> userModelArrayList = new ArrayList<>();
        for (UserDTO userDTO : userDTOList) {
            userModelArrayList.add(map(userDTO));
        }
        return userModelArrayList;
    }

    public static UserModel map(UserDTO userDTO) {
        UserModel userModel = new UserModel();
        if (userDTO.getuPseudo() != null) {
            userModel.setuPseudo(userDTO.getuPseudo());
        } else {
            userModel.setuPseudo("?????");
        }
        if (userDTO.getuPseudo() != null) {
            userModel.setEmail(userDTO.getEmail());
        } else {
            userModel.setEmail("Indisponible");
        }
        if (userDTO.getuPseudo() != null) {
            userModel.setuAccessType(userDTO.getuAccessType());
        } else {
            userModel.setuAccessType("user");
        }
        userModel.setuCreationDate(userDTO.getDate());
        userModel.setId(userDTO.getId());
        userModel.setuPassword(userDTO.getuPassword());
        return userModel;
    }
}
