package com.example.savking_sample.Mapper;

import com.example.savking_sample.DTO.IncomeDTO;
import com.example.savking_sample.models.IncomeModel;

import java.util.ArrayList;
import java.util.List;

public class IncomeMapper {

    public static List<IncomeModel> map(List<IncomeDTO> incomeDTOList) {
        List<IncomeModel> incomeModelList = new ArrayList<>();
        for (IncomeDTO incomeDTO : incomeDTOList) {
            incomeModelList.add(map(incomeDTO));
        }
        return incomeModelList;
    }


    private static IncomeModel map(IncomeDTO incomeDTO) {

        IncomeModel incomeModel = new IncomeModel();
        incomeModel.setiTitle(incomeDTO.getiTitle());
        if(incomeDTO.getiDescription() != null && !incomeDTO.getiDescription().matches("")) {
            incomeModel.setiDescription(incomeDTO.getiDescription());
        } else {
            incomeModel.setiDescription("Description non renseignée");
        }
        if(Double.toString(incomeDTO.getiGreenCash())!= null) {
            incomeModel.setiGreenCash(incomeDTO.getiGreenCash());
        } else {
            incomeModel.setiGreenCash(0.00);
        }
        if(Double.toString(incomeDTO.getiCreditCard()) != null) {
            incomeModel.setiCreditCard(incomeDTO.getiCreditCard());
        } else {
            incomeModel.setiCreditCard(0.00);
        }
        if(Double.toString(incomeDTO.getiTr()) != null) {
            incomeModel.setiTr(incomeDTO.getiTr());
        } else {
            incomeModel.setiTr(0.00);
        }
        incomeModel.setiIdUser(incomeDTO.getiIdUser());
        incomeModel.setDate(incomeDTO.getDate());
        incomeModel.setId(incomeDTO.getId());
        if(incomeDTO.getCategory() != null){
            incomeModel.setCategory(incomeDTO.getCategory());
        }
        return incomeModel;
    }

}
