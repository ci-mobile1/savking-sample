package com.example.savking_sample.Mapper;


import com.example.savking_sample.DTO.CashDTO;
import com.example.savking_sample.models.CashModel;

import java.util.ArrayList;
import java.util.List;

public class CashMapper {
    public static List<CashModel> map(List<CashDTO> cashDTOList) {
        List<CashModel> cashModelList = new ArrayList<>();
        for (CashDTO cashDTO : cashDTOList) {
            cashModelList.add(map(cashDTO));
        }
        return cashModelList;
    }

    private static CashModel map(CashDTO cashDTO) {
        CashModel cashModel = new CashModel();
        cashModel.setCreditCard(cashDTO.getCreditCard());
        cashModel.setGreenCash(cashDTO.getGreenCash());
        cashModel.setCategory(cashDTO.getCategory());
        cashModel.setTitle(cashDTO.getTitle());
        cashModel.setDate(cashDTO.getDate());
        cashModel.setcTR(cashDTO.getcTR());
        cashModel.setDescription(cashDTO.getDescription());
        cashModel.setIdUser(cashDTO.getIdUser());
        cashModel.setId(cashDTO.getId());
        return cashModel;
    }
}
