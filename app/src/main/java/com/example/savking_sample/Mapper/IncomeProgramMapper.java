package com.example.savking_sample.Mapper;
import com.example.savking_sample.DTO.IncomeProgramDTO;
import com.example.savking_sample.models.IncomeProgramModel;

import java.util.ArrayList;
import java.util.List;

public class IncomeProgramMapper {
    public static List<IncomeProgramModel> map(List<IncomeProgramDTO> incomeCategoryDTOS) {
        List<IncomeProgramModel> incomeProgramModels = new ArrayList<>();
        for (IncomeProgramDTO incomeProgramDTO : incomeCategoryDTOS) {
            incomeProgramModels.add(map(incomeProgramDTO));
        }
        return incomeProgramModels;
    }

    private static IncomeProgramModel map(IncomeProgramDTO incomeProgramDTO) {
        IncomeProgramModel incomeProgramModel = new IncomeProgramModel();
        incomeProgramModel.setiCategory(incomeProgramDTO.getiCategory());
        incomeProgramModel.setiCreditCard(incomeProgramDTO.getiCreditCard());
        incomeProgramModel.setiGreenCash(incomeProgramDTO.getiGreenCash());
        incomeProgramModel.setiTR(incomeProgramDTO.getiTR());
        incomeProgramModel.setiDateBegin(incomeProgramDTO.getiDateBegin());
        incomeProgramModel.setiLastTimeAdded(incomeProgramDTO.getiLastTimeAdded());
        incomeProgramModel.setiTypeOfProgram(incomeProgramDTO.getiTypeOfProgram());
        incomeProgramModel.setiIdUser(incomeProgramDTO.getiIdUser());
        incomeProgramModel.setiTitle(incomeProgramDTO.getiTitle());
        incomeProgramModel.set_id(incomeProgramDTO.getId());
        incomeProgramModel.setiDescription(incomeProgramDTO.getiDescription());
        incomeProgramModel.setiSwitch(incomeProgramDTO.isiSwitch());
        incomeProgramModel.setiDateEnd(incomeProgramDTO.getiDateEnd());
        return incomeProgramModel;
    }
}
