package com.example.savking_sample.Mapper;


import com.example.savking_sample.DTO.CategoryDTO;
import com.example.savking_sample.models.CategoryModel;

import java.util.ArrayList;
import java.util.List;

public class CategoryMapper {

    public static List<CategoryModel> map(List<CategoryDTO> categoryDTOList) {
        List<CategoryModel> categoryModelArrayList = new ArrayList<>();
        for (CategoryDTO categoryDTO : categoryDTOList) {
            categoryModelArrayList.add(map(categoryDTO));
        }
        return categoryModelArrayList;
    }

    private static CategoryModel map(CategoryDTO categoryDTO) {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setCategoryName(categoryDTO.getCategoryName());
        categoryModel.setId(categoryDTO.getId());
        categoryModel.setIdUser(categoryDTO.getIdUser());
        categoryModel.setCategoryType(categoryDTO.getCategoryType());
        return categoryModel;
    }
}
