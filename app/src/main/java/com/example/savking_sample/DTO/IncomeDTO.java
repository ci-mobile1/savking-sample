package com.example.savking_sample.DTO;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class IncomeDTO {

    @SerializedName("iCreditCard") private double iCreditCard;
    @SerializedName("iGreenincome") private double iGreenCash;
    @SerializedName("iTr") private double iTr;
    @SerializedName("iDescription") private String iDescription;
    @SerializedName("iTitle") private String iTitle;
    @SerializedName("iCategory") private String category;
    @SerializedName("iIdUser") private String iIdUser;
    @SerializedName("date") private Date date;
    @SerializedName("_id") private String id;


    public double getiCreditCard() {
        return iCreditCard;
    }

    public void setiCreditCard(double iCreditCard) {
        this.iCreditCard = iCreditCard;
    }

    public double getiGreenCash() {
        return iGreenCash;
    }

    public void setiGreenCash(double iGreenCash) {
        this.iGreenCash = iGreenCash;
    }

    public double getiTr() {
        return iTr;
    }

    public void setiTr(double iTr) {
        this.iTr = iTr;
    }

    public String getiDescription() {
        return iDescription;
    }

    public void setiDescription(String iDescription) {
        this.iDescription = iDescription;
    }

    public String getiTitle() {
        return iTitle;
    }

    public void setiTitle(String iTitle) {
        this.iTitle = iTitle;
    }

    public String getiIdUser() {
        return iIdUser;
    }

    public void setiIdUser(String iIdUser) {
        this.iIdUser = iIdUser;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "IncomeDTO{" +
                "iCreditCard=" + iCreditCard +
                ", iGreenCash=" + iGreenCash +
                ", iTr=" + iTr +
                ", iDescription='" + iDescription + '\'' +
                ", iTitle='" + iTitle + '\'' +
                ", category='" + category + '\'' +
                ", iIdUser='" + iIdUser + '\'' +
                ", date='" + date + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
