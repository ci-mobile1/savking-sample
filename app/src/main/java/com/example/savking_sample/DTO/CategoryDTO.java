package com.example.savking_sample.DTO;

import com.google.gson.annotations.SerializedName;

public class CategoryDTO {

    @SerializedName("cIdUser") private String idUser;
    @SerializedName("_id") private String id;
    @SerializedName("categoryType") private int categoryType;
    @SerializedName("categoryName") private String categoryName;

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(int categoryType) {
        this.categoryType = categoryType;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return "CategoryDTO{" +
                "idUser='" + idUser + '\'' +
                ", id='" + id + '\'' +
                ", categoryType='" + categoryType + '\'' +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }
}
