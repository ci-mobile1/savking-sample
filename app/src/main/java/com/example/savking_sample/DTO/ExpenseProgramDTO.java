package com.example.savking_sample.DTO;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ExpenseProgramDTO {
    @SerializedName("eGreenCash") private double eGreenCash;
    @SerializedName("eCreditCard") private double eCreditCard;
    @SerializedName("eTR") private double eTR;
    @SerializedName("eDescription") private String eDescription;
    @SerializedName("eCategory") private String eCategory;
    @SerializedName("eTitle") private String eTitle;
    @SerializedName("eIdUser") private String eIdUser;
    @SerializedName("eDateBegin") private Date eDateBegin;
    @SerializedName("eDateEnd") private Date eDateEnd;
    @SerializedName("eSwitch") private boolean eSwitch;
    @SerializedName("eLastTimeAdded") private Date eLastTimeAdded;
    @SerializedName("eTypeOfProgram") private Date eTypeOfProgram;
    @SerializedName("_id") private String id;
    @SerializedName("isAddNow") private boolean isAddNow;


    public double geteGreenCash() {
        return eGreenCash;
    }

    public void seteGreenCash(double eGreenCash) {
        this.eGreenCash = eGreenCash;
    }

    public double geteCreditCard() {
        return eCreditCard;
    }

    public void seteCreditCard(double eCreditCard) {
        this.eCreditCard = eCreditCard;
    }

    public double geteTR() {
        return eTR;
    }

    public void seteTR(double eTR) {
        this.eTR = eTR;
    }

    public String geteDescription() {
        return eDescription;
    }

    public void seteDescription(String eDescription) {
        this.eDescription = eDescription;
    }

    public String geteCategory() {
        return eCategory;
    }

    public void seteCategory(String eCategory) {
        this.eCategory = eCategory;
    }

    public String geteTitle() {
        return eTitle;
    }

    public void seteTitle(String eTitle) {
        this.eTitle = eTitle;
    }

    public String geteIdUser() {
        return eIdUser;
    }

    public void seteIdUser(String eIdUser) {
        this.eIdUser = eIdUser;
    }

    public Date geteDateBegin() {
        return eDateBegin;
    }

    public void seteDateBegin(Date eDateBegin) {
        this.eDateBegin = eDateBegin;
    }

    public boolean iseSwitch() {
        return eSwitch;
    }

    public void seteSwitch(boolean eSwitch) {
        this.eSwitch = eSwitch;
    }

    public Date geteLastTimeAdded() {
        return eLastTimeAdded;
    }

    public void seteLastTimeAdded(Date eLastTimeAdded) {
        this.eLastTimeAdded = eLastTimeAdded;
    }

    public Date geteTypeOfProgram() {
        return eTypeOfProgram;
    }

    public void seteTypeOfProgram(Date eTypeOfProgram) {
        this.eTypeOfProgram = eTypeOfProgram;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date geteDateEnd() {
        return eDateEnd;
    }

    public void seteDateEnd(Date eDateEnd) {
        this.eDateEnd = eDateEnd;
    }

    public boolean isAddNow() {
        return isAddNow;
    }

    public void setAddNow(boolean addNow) {
        isAddNow = addNow;
    }

    @Override
    public String toString() {
        return "ExpenseProgramDTO{" +
                "eGreenCash=" + eGreenCash +
                ", eCreditCard=" + eCreditCard +
                ", eTR=" + eTR +
                ", eDescription='" + eDescription + '\'' +
                ", eCategory='" + eCategory + '\'' +
                ", eTitle='" + eTitle + '\'' +
                ", eIdUser='" + eIdUser + '\'' +
                ", eDateBegin=" + eDateBegin +
                ", eDateEnd=" + eDateEnd +
                ", eSwitch=" + eSwitch +
                ", eLastTimeAdded=" + eLastTimeAdded +
                ", eTypeOfProgram=" + eTypeOfProgram +
                ", id=" + id +
                ", isAddNow=" + isAddNow +
                '}';
    }
}