package com.example.savking_sample.DTO;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class CashDTO {

    @SerializedName("cGreenCash") private double greenCash;
    @SerializedName("cCategory") private String category;
    @SerializedName("cTitle") private String title;
    @SerializedName("cCreditCard") private double creditCard;
    @SerializedName("date") private Date date;
    @SerializedName("cDescription") private String description;
    @SerializedName("cTR") private double cTR;
    @SerializedName("_id") private String id;
    @SerializedName("cIdUser") private String idUser;

    public double getGreenCash() {
        return greenCash;
    }

    public void setGreenCash(double greenCash) {
        this.greenCash = greenCash;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(double creditCard) {
        this.creditCard = creditCard;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getcTR() {
        return cTR;
    }

    public void setcTR(double cTR) {
        this.cTR = cTR;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    @Override
    public String toString() {
        return "CashDTO{" +
                "greenCash=" + greenCash +
                ", category='" + category + '\'' +
                ", title='" + title + '\'' +
                ", creditCard=" + creditCard +
                ", date='" + date + '\'' +
                ", description='" + description + '\'' +
                ", cTR=" + cTR +
                ", id='" + id + '\'' +
                ", idUser='" + idUser + '\'' +
                '}';
    }
}
