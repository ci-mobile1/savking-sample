package com.example.savking_sample.DTO;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class IncomeProgramDTO {
    @SerializedName("iGreenIncome") private double iGreenCash;
    @SerializedName("iCreditCard") private double iCreditCard;
    @SerializedName("iTR") private double iTR;
    @SerializedName("iDescription") private String iDescription;
    @SerializedName("iCategory") private String iCategory;
    @SerializedName("iTitle") private String iTitle;
    @SerializedName("iIdUser") private String iIdUser;
    @SerializedName("iDateBegin") private Date iDateBegin;
    @SerializedName("iDateEnd") private Date iDateEnd;
    @SerializedName("iSwitch") private boolean iSwitch;
    @SerializedName("iLastTimeAdded") private Date iLastTimeAdded;
    @SerializedName("iTypeOfProgram") private Date iTypeOfProgram;
    @SerializedName("_id") private String id;
    @SerializedName("isAddNow") private boolean isAddNow;

    public double getiGreenCash() {
        return iGreenCash;
    }

    public void setiGreenCash(double iGreenCash) {
        this.iGreenCash = iGreenCash;
    }

    public double getiCreditCard() {
        return iCreditCard;
    }

    public void setiCreditCard(double iCreditCard) {
        this.iCreditCard = iCreditCard;
    }

    public double getiTR() {
        return iTR;
    }

    public void setiTR(double iTR) {
        this.iTR = iTR;
    }

    public String getiDescription() {
        return iDescription;
    }

    public void setiDescription(String iDescription) {
        this.iDescription = iDescription;
    }

    public String getiCategory() {
        return iCategory;
    }

    public void setiCategory(String iCategory) {
        this.iCategory = iCategory;
    }

    public String getiTitle() {
        return iTitle;
    }

    public void setiTitle(String iTitle) {
        this.iTitle = iTitle;
    }

    public String getiIdUser() {
        return iIdUser;
    }

    public void setiIdUser(String iIdUser) {
        this.iIdUser = iIdUser;
    }

    public Date getiDateBegin() {
        return iDateBegin;
    }

    public void setiDateBegin(Date eDateBegin) {
        this.iDateBegin = eDateBegin;
    }

    public Date getiDateEnd() {
        return iDateEnd;
    }

    public void setiDateEnd(Date iDateEnd) {
        this.iDateEnd = iDateEnd;
    }

    public boolean isiSwitch() {
        return iSwitch;
    }

    public void seteSwitch(boolean eSwitch) {
        this.iSwitch = eSwitch;
    }

    public Date getiLastTimeAdded() {
        return iLastTimeAdded;
    }

    public void setiLastTimeAdded(Date iLastTimeAdded) {
        this.iLastTimeAdded = iLastTimeAdded;
    }

    public Date getiTypeOfProgram() {
        return iTypeOfProgram;
    }

    public void setiTypeOfProgram(Date iTypeOfProgram) {
        this.iTypeOfProgram = iTypeOfProgram;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAddNow() {
        return isAddNow;
    }

    public void setAddNow(boolean addNow) {
        isAddNow = addNow;
    }

    @Override
    public String toString() {
        return "IncomeProgramDTO{" +
                "iGreenCash=" + iGreenCash +
                ", iCreditCard=" + iCreditCard +
                ", iTR=" + iTR +
                ", iDescription='" + iDescription + '\'' +
                ", iCategory='" + iCategory + '\'' +
                ", iTitle='" + iTitle + '\'' +
                ", iIdUser='" + iIdUser + '\'' +
                ", eDateBegin=" + iDateBegin +
                ", iDateEnd=" + iDateEnd +
                ", eSwitch=" + iSwitch +
                ", iLastTimeAdded=" + iLastTimeAdded +
                ", iTypeOfProgram=" + iTypeOfProgram +
                ", id=" + id +
                ", isAddNow=" + isAddNow +
                '}';
    }
}