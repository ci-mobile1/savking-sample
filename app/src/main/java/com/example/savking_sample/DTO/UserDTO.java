package com.example.savking_sample.DTO;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class UserDTO {

    @SerializedName("uEmailAdress") private String email;
    @SerializedName("uAccessType") private String uAccessType;
    @SerializedName("uPassword") private String uPassword;
    @SerializedName("uPseudo") private String uPseudo;
    @SerializedName("_id") private String id;
    @SerializedName("date") private Date date;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getuAccessType() {
        return uAccessType;
    }

    public void setuAccessType(String uAccessType) {
        this.uAccessType = uAccessType;
    }

    public String getuPassword() {
        return uPassword;
    }

    public void setuPassword(String uPassword) {
        this.uPassword = uPassword;
    }

    public String getuPseudo() {
        return uPseudo;
    }

    public void setuPseudo(String uPseudo) {
        this.uPseudo = uPseudo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "email='" + email + '\'' +
                ", uAccessType='" + uAccessType + '\'' +
                ", uPassword='" + uPassword + '\'' +
                ", uPseudo='" + uPseudo + '\'' +
                ", id='" + id + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
