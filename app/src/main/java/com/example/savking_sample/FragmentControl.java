package com.example.savking_sample;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import java.util.Objects;

public class FragmentControl<T> {

    private static FragmentControl fragmentControl;
    private T data;

    static public void fragmentTransaction(Fragment fragment, FragmentManager fragmentManager, Bundle bundle, String stackTag ) {
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.nav_host_fragment, fragment);
        fragmentTransaction.addToBackStack(stackTag).commit();
    }
    static public void fragmentTransactionPop(Fragment fragment, FragmentManager fragmentManager, Bundle bundle) {
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = Objects.requireNonNull(fragmentManager).beginTransaction();
        fragmentManager.popBackStack();
        fragmentTransaction.replace(R.id.nav_host_fragment, fragment);
        fragmentTransaction.commit();
    }


    public static FragmentControl getInstance() {
        if (fragmentControl == null) {
            fragmentControl = new FragmentControl();
        }
        return fragmentControl;
    }
/*
    public static <T> T getData(T array) {
        return data;
    }
*/


}
