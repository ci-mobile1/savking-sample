package com.example.savking_sample.services;


import com.example.savking_sample.DTO.IncomeDTO;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiCallIncome {

    @GET("getIncomes")
    Call<List<IncomeDTO>> getIncomes();

    @POST("getIncomeByMonth")
    @Headers("Content-Type: application/json")
    Call<List<IncomeDTO>> getIncomeByMonth(@Body JsonObject body);

    @POST("getIncomeByActualMonth")
    @Headers("Content-Type: application/json")
    Call<List<IncomeDTO>> getIncomeByActualMonth(@Body JsonObject body);

    @GET("getIncomeById/{_id}")
    Call<List<IncomeDTO>> getIncomeById();

    @PUT("updateIncomeById/{id}")
    Call<IncomeDTO> updateIncome(@Path("id") String id, @Body IncomeDTO incomeDTO);

    @DELETE("deleteIncomeById/{id}")
    Call<IncomeDTO> deleteIncome(@Path("id") String id);

    @Headers("Content-Type: application/json")
    @POST("newIncome")
    Call<IncomeDTO> addNewIncome(@Body IncomeDTO incomeDTO);


}
