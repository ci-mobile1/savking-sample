package com.example.savking_sample.services;

import com.example.savking_sample.DTO.ExpenseProgramDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiCallExpenseProgram {

    @POST("newExpenseProgrammed")
    @Headers("Content-Type: application/json")
    Call<ExpenseProgramDTO> addNewExpenseProgrammed (@Body ExpenseProgramDTO expenseProgramDTO);

    @GET("getExpenseProgrammed")
    Call<List<ExpenseProgramDTO>> getExpenseProgrammedByUser(@Header("token") String token);


    @GET("getExpenseProgramInMonth")
    Call<List<ExpenseProgramDTO>> getExpenseProgramInMonth(@Header("token") String token);

    @PUT("updateExpensePogrammed/{id}")
    Call<ExpenseProgramDTO> updateExpenseProgram(@Path("id") String id, @Body ExpenseProgramDTO expenseProgramDTO);

    @DELETE("deleteExpenseProgrammmed/{id}")
    Call<ExpenseProgramDTO>deleteExpenseProgram(@Path("id") String id);

}
