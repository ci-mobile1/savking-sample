package com.example.savking_sample.services;

import com.example.savking_sample.DTO.CashDTO;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiCallCash {

    @POST("getMoneyLeft")
    @Headers("Content-Type: application/json")
    Call<JsonObject> getMoneyLeft(@Body JsonObject body);

    @POST("getCashByMonth")
    @Headers("Content-Type: application/json")
    Call<List<CashDTO>> getCashByMonth(@Body JsonObject body);

    @POST("getCashByActualMonth")
    @Headers("Content-Type: application/json")
    Call<List<CashDTO>> getCashByActualMonth(@Body JsonObject body);

    @GET("getCashById/{_id}")
    Call<List<CashDTO>> getCashById(@Path("id") String id);

    @PUT("updateCashById/{id}")
    Call<CashDTO> updateCash(@Path("id") String id, @Body CashDTO cashDTO);

    @DELETE("deleteCashById/{id}")
    //Call<List<CashDTO>> deleteCash(@Query());
    Call<CashDTO> deleteCash(@Path("id") String id);

    @Headers("Content-Type: application/json")
    @POST("newCash")
    Call<CashDTO> addNewCash(@Body CashDTO cashDTO);

    @POST("auth/login")
    @Headers("Content-Type: application/json")
    Call<JsonObject> login(@Body JsonObject body);

    @POST("getSavingByYear")
    @Headers("Content-Type: application/json")
    Call<JsonObject> getSavingByYear(@Body JsonObject body);


    @POST("auth/verifConnected")
    @Headers("Content-Type: application/json")
    Call<JsonObject> verfifConnected(@Body JsonObject body);


}

