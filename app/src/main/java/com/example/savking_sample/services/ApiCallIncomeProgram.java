package com.example.savking_sample.services;
import com.example.savking_sample.DTO.IncomeProgramDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiCallIncomeProgram {

    @POST("newIncomeProgrammed")
    @Headers("Content-Type: application/json")
    Call<IncomeProgramDTO> addNewExpenseProgrammed (@Body IncomeProgramDTO incomeProgramDTO);

    @GET("getIncomeProgrammed")
    Call<List<IncomeProgramDTO>> getIncomeProgrammedByUser(@Header("token") String token);

    @GET("getIncomeProgramInMonth")
    Call<List<IncomeProgramDTO>> getIncomeProgramInMonth(@Header("token") String token);


    @PUT("updateIncomePogrammed/{id}")
    Call<IncomeProgramDTO> updateIncomeProgram(@Path("id") String id, @Body IncomeProgramDTO incomeProgramDTO);

    @DELETE("deleteIncomeProgrammmed/{id}")
    Call<IncomeProgramDTO>deleteIncomeProgram(@Path("id") String id);

}
