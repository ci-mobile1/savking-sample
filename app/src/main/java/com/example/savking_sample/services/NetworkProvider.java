package com.example.savking_sample.services;

import android.util.Log;

import androidx.annotation.NonNull;


import com.example.savking_sample.DTO.CashDTO;
import com.example.savking_sample.DTO.CategoryDTO;
import com.example.savking_sample.DTO.ExpenseProgramDTO;
import com.example.savking_sample.DTO.IncomeDTO;
import com.example.savking_sample.DTO.IncomeProgramDTO;
import com.example.savking_sample.DTO.UserDTO;
import com.example.savking_sample.Mapper.CashMapper;
import com.example.savking_sample.Mapper.CategoryMapper;
import com.example.savking_sample.Mapper.ExpenseProgramMapper;
import com.example.savking_sample.Mapper.IncomeMapper;
import com.example.savking_sample.Mapper.IncomeProgramMapper;
import com.example.savking_sample.Mapper.UserMapper;
import com.example.savking_sample.models.CashModel;
import com.example.savking_sample.models.CategoryModel;
import com.example.savking_sample.models.ExpenseProgramModel;
import com.example.savking_sample.models.IncomeModel;
import com.example.savking_sample.models.IncomeProgramModel;
import com.example.savking_sample.models.UserModel;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class NetworkProvider {
  ApiCallCash apiCallCash;
  ApiCallIncome apiCallIncome;
  ApiCallCategory apiCallCategory;
  ApiCallUser apiCallUser;
  ApiCallExpenseProgram apiCallExpenseProgram;
  ApiCallIncomeProgram apiCallIncomeProgram;

  private static com.example.savking_sample.services.NetworkProvider instance;

  public static com.example.savking_sample.services.NetworkProvider getInstance() {
    if (instance == null) {
      instance = new com.example.savking_sample.services.NetworkProvider();
    }
    return instance;
  }


  public interface Listener <T> {
    void onSuccess(List<T> data);

    void onError(Throwable t);
  }
  public interface Listen <T> {
    void onSuccess(JsonObject data);

    void onError(Throwable t);
  }
  public interface ListenUser <T> {
    void onSuccess(UserModel data);

    void onError(Throwable t);
  }
  public interface ListenerIncome <T> {
    void onSuccess(List<IncomeModel> data);

    void onError(Throwable t);
  }

  public interface ListenerCategory <T> {
    void onSuccess(List<CategoryModel> data);

    void onError(Throwable t);
  }



  private NetworkProvider() {
   Retrofit retrofit = new Retrofit.Builder().baseUrl("https://appsapicash.herokuapp.com/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    apiCallCash = retrofit.create(ApiCallCash.class);
    apiCallIncome = retrofit.create(ApiCallIncome.class);
    apiCallUser = retrofit.create(ApiCallUser.class);
    apiCallCategory = retrofit.create(ApiCallCategory.class);
    apiCallExpenseProgram = retrofit.create(ApiCallExpenseProgram.class);
    apiCallIncomeProgram = retrofit.create(ApiCallIncomeProgram.class);

  }

  public void getCashByMonth(String token, String idUser, int monthWanted, int yearWanted, final Listener<CashModel> listener) {

    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("token", token);
    jsonObject.addProperty("cIdUser", idUser);
    jsonObject.addProperty("monthWanted", monthWanted);
    jsonObject.addProperty("yearWanted", yearWanted);

    Call<List<CashDTO>> call = apiCallCash.getCashByMonth(jsonObject);

    call.enqueue(new Callback<List<CashDTO>>() {

      @Override
      public void onResponse(@NonNull Call<List<CashDTO>> call, @NonNull Response<List<CashDTO>> response) {
        List<CashDTO> cashDTOS = response.body();

        List<CashModel> cashModelList = null;
        if (cashDTOS != null) {
          cashModelList = CashMapper.map(cashDTOS);
        }

        listener.onSuccess(cashModelList);

      }

      @Override
      public void onFailure(Call<List<CashDTO>> call, Throwable t) {

        Log.d("testPost","onFail NETWORK :");
        Log.d("testPost",t.toString());

      }
    });

  }

  public void getMoneyLeft(String token, String idUser, final Listen<JsonObject> listener) {

    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("cIdUser", idUser);
    jsonObject.addProperty("token", token);

    Call<JsonObject> call = apiCallCash.getMoneyLeft(jsonObject);

    call.enqueue(new Callback<JsonObject>() {

      @Override
      public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
        JsonObject cashDTOS = response.body();

        //System.out.println(response.body().toString());
        listener.onSuccess(cashDTOS);

      }

      @Override
      public void onFailure(Call<JsonObject> call, Throwable t) {

        Log.d("testPost",t.toString());

      }
    });

  }

  public void getCashByActualMonth(String token, String idUser, final Listener<CashModel> listener) {

    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("token", token);
    jsonObject.addProperty("cIdUser", idUser);

    Call<List<CashDTO>> call = apiCallCash.getCashByActualMonth(jsonObject);

    call.enqueue(new Callback<List<CashDTO>>() {

      @Override
      public void onResponse(@NonNull Call<List<CashDTO>> call, @NonNull Response<List<CashDTO>> response) {
        List<CashDTO> cashDTOS = response.body();
        List<CashModel> cashModelList = null;
        if (cashDTOS != null) {
          cashModelList = CashMapper.map(cashDTOS);

        }
        //System.out.println(response.body().toString());
        listener.onSuccess(cashModelList);

      }

      @Override
      public void onFailure(Call<List<CashDTO>> call, Throwable t) {

        Log.d("testPost",t.toString());

      }
    });

  }

  public void addNewCash(CashDTO cashDTO){

    Call<CashDTO> call = apiCallCash.addNewCash(cashDTO);
    call.enqueue(new Callback<CashDTO>() {

      @Override
      public void onResponse(Call<CashDTO> call, Response<CashDTO> response) {
        if(response.isSuccessful()){
          Log.d("testPost","that's work ");
          Log.d("testPost",response.body().toString());
        }

      }


      @Override
      public void onFailure(Call<CashDTO> call, Throwable t) {

        Log.d("testPost","error :");
        Log.d("testPost",t.toString());

      }
    });

  }

  public void updateCashById(final CashDTO cashDTO){
    apiCallCash.updateCash(cashDTO.getId(), cashDTO).enqueue(new Callback<CashDTO>() {
      @Override
      public void onResponse(Call<CashDTO> call, Response<CashDTO> response) {

      }
      @Override
      public void onFailure(Call<CashDTO> call, Throwable t) {

        Log.e("ResponseDelete", t.toString());
      }
    });

  }

  public void deleteCash(final String idExepense){
    apiCallCash.deleteCash(idExepense).enqueue(new Callback<CashDTO>() {
      @Override
      public void onResponse(Call<CashDTO> call, Response<CashDTO> response) {
        Log.d("ResponseDelete", Integer.toString(response.code()));
        Log.d("ResponseDelete",call.request().toString());


      }
      @Override
      public void onFailure(Call<CashDTO> call, Throwable t) {

        Log.e("ResponseDelete", t.toString());
      }
    });

  }

  public Listen<JsonObject> getSavingByYear(int yearExpected, String idUser, final Listen<JsonObject> listen) {

    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("yearExpected", yearExpected);
    jsonObject.addProperty("idUser", idUser);


    Call <JsonObject> call = apiCallCash.getSavingByYear(jsonObject);
    call.enqueue(new Callback <JsonObject>() {
      @Override
      public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {

        JsonObject savingInTheYear = response.body();

        listen.onSuccess(savingInTheYear);

      }

      @Override
      public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
        JsonObject list = new JsonObject();
        list.addProperty("idUser", "");
      }
    });
    return listen;
  }

  //Incomes
  public void getIncomeByMonth(String token, String idUser, int monthWanted, int yearWanted, final Listener<IncomeModel> listener) {

    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("token", token);
    jsonObject.addProperty("iIdUser", idUser);
    jsonObject.addProperty("monthWanted", monthWanted);
    jsonObject.addProperty("yearWanted", yearWanted);

    Call<List<IncomeDTO>> call = apiCallIncome.getIncomeByMonth(jsonObject);

    call.enqueue(new Callback<List<IncomeDTO>>() {

      @Override
      public void onResponse(@NonNull Call<List<IncomeDTO>> call, @NonNull Response<List<IncomeDTO>> response) {
        List<IncomeDTO> incomeDTOS = response.body();

        List<IncomeModel> incomeModelList = null;
        if (incomeDTOS != null) {
          incomeModelList = IncomeMapper.map(incomeDTOS);
        }

        listener.onSuccess(incomeModelList);

      }

      @Override
      public void onFailure(Call<List<IncomeDTO>> call, Throwable t) {

      }
    });

  }

  public void getIncomeByActualMonth(String token, String idUser, final ListenerIncome<IncomeModel> listener) {

    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("token", token);
    jsonObject.addProperty("iIdUser", idUser);

    Call<List<IncomeDTO>> call = apiCallIncome.getIncomeByActualMonth(jsonObject);

    call.enqueue(new Callback<List<IncomeDTO>>() {

      @Override
      public void onResponse(@NonNull Call<List<IncomeDTO>> call, @NonNull Response<List<IncomeDTO>> response) {
        List<IncomeDTO> incomeDTOS = response.body();
        List<IncomeModel> incomeModelList = null;
        if (incomeDTOS != null) {
          incomeModelList = IncomeMapper.map(incomeDTOS);
          Log.d("AHAHAHAAHAHAHAHAHAH", "onResponse: " + incomeModelList);
        }

        listener.onSuccess(incomeModelList);

      }

      @Override
      public void onFailure(Call<List<IncomeDTO>> call, Throwable t) {
        Log.d("Fail_incomeByMonth", t.toString());
      }
    });
  }

  public void addNewIncome(IncomeDTO incomeDTO){
    Call<IncomeDTO> call = apiCallIncome.addNewIncome(incomeDTO);
    call.enqueue(new Callback<IncomeDTO>() {
      @Override
      public void onResponse(Call<IncomeDTO> call, Response<IncomeDTO> response) {
        if(!response.isSuccessful()){
          Log.d("fail_AddIncome","error");
        }

      }


      @Override
      public void onFailure(Call<IncomeDTO> call, Throwable t) {

        Log.d("onFailure_AddIncome","error :" + t);


      }
    });

  }

  public void updateIncomeById(final IncomeDTO incomeDTO){
    apiCallIncome.updateIncome(incomeDTO.getId(), incomeDTO).enqueue(new Callback<IncomeDTO>() {
      @Override
      public void onResponse(Call<IncomeDTO> call, Response<IncomeDTO> response) {

      }
      @Override
      public void onFailure(Call<IncomeDTO> call, Throwable t) {

        Log.e("OnFailureUptadeIncome", t.toString());
      }
    });

  }

  public void deleteIncome(final String idIncome){
    apiCallIncome.deleteIncome(idIncome).enqueue(new Callback<IncomeDTO>() {
      @Override
      public void onResponse(Call<IncomeDTO> call, Response<IncomeDTO> response) {

      }
      @Override
      public void onFailure(Call<IncomeDTO> call, Throwable t) {

        Log.e("OnFailureDeleteIncome", t.toString());
      }
    });

  }

  //Income Program
  public void getIncomeProgramOnActualMonth(String token, final Listener<IncomeProgramModel> listener) {



    Call<List<IncomeProgramDTO>> call = apiCallIncomeProgram.getIncomeProgramInMonth(token);

    call.enqueue(new Callback<List<IncomeProgramDTO>>() {

      @Override
      public void onResponse(@NonNull Call<List<IncomeProgramDTO>> call, @NonNull Response<List<IncomeProgramDTO>> response) {
        List<IncomeProgramDTO> incomeProgramDTOS = response.body();
        List<IncomeProgramModel> incomeProgramModels = null;
        if (incomeProgramDTOS != null) {
          incomeProgramModels = IncomeProgramMapper.map(incomeProgramDTOS);

        }
        //System.out.println(response.body().toString());
        listener.onSuccess(incomeProgramModels);

      }

      @Override
      public void onFailure(Call<List<IncomeProgramDTO>> call, Throwable t) {

        Log.d("testPost",t.toString());

      }
    });

  }

  // Expense Program

  public void getExpenseProgramOnActualMonth(String token, final Listener<ExpenseProgramModel> listener) {

    Call<List<ExpenseProgramDTO>> call = apiCallExpenseProgram.getExpenseProgramInMonth(token);

    call.enqueue(new Callback<List<ExpenseProgramDTO>>() {

      @Override
      public void onResponse(@NonNull Call<List<ExpenseProgramDTO>> call, @NonNull Response<List<ExpenseProgramDTO>> response) {
        List<ExpenseProgramDTO> expenseProgramDTOS = response.body();
        List<ExpenseProgramModel> expenseProgramModels = null;
        if (expenseProgramDTOS != null) {
          expenseProgramModels = ExpenseProgramMapper.map(expenseProgramDTOS);

        }
        //System.out.println(response.body().toString());
        listener.onSuccess(expenseProgramModels);

      }

      @Override
      public void onFailure(Call<List<ExpenseProgramDTO>> call, Throwable t) {

        Log.d("ErrExpProgGETMONTH",t.toString());

      }
    });

  }



  //Login

  public Listen<JsonObject> login(String email, String password, final Listen<JsonObject> listen) {

    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("uEmailAdress", email);
    jsonObject.addProperty("uPassword", password);


    Call <JsonObject> call = apiCallCash.login(jsonObject);
    call.enqueue(new Callback <JsonObject>() {
      @Override
      public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {

          JsonObject list = response.body();

          listen.onSuccess(list);

      }

      @Override
      public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
        JsonObject list = new JsonObject();
        list.addProperty("idUser", "");
      }
    });
    return listen;
  }

  public Listen<JsonObject> verifConnected(String token, final Listen<JsonObject> listen) {

    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("token", token);

    Call <JsonObject> call = apiCallCash.verfifConnected(jsonObject);
    call.enqueue(new Callback <JsonObject>() {
      @Override
      public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
        JsonObject list = response.body();
          //Log.d("LOGIN", list.toString());
        listen.onSuccess(list);
      }

      @Override
      public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
        JsonObject list = new JsonObject();
        list.addProperty("connected", "false");
      }
    });
    return listen;
  }

//Category

  public Listen<JsonObject> AddNewCategory(CategoryDTO categoryDTO, final Listen<JsonObject> listen) {


    Call <JsonObject> call = apiCallCategory.addNewCategory(categoryDTO);
    call.enqueue(new Callback <JsonObject>() {
      @Override
      public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {

        JsonObject list = response.body();

        listen.onSuccess(list);

      }

      @Override
      public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {

      }
    });
    return listen;
  }


  public ListenerCategory<CategoryModel> getCategoriesByUser(String idUser, String token, int categoryType, final ListenerCategory<CategoryModel> listen) {

    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("cIdUser", idUser);
    jsonObject.addProperty("token", token);
    jsonObject.addProperty("categoryType", categoryType);



    Call <List<CategoryDTO>> call = apiCallCategory.getCategoriesByUser(jsonObject);
    call.enqueue(new Callback <List<CategoryDTO>>() {
      @Override
      public void onResponse(@NonNull Call<List<CategoryDTO>> call, @NonNull Response<List<CategoryDTO>> response) {
        List<CategoryDTO> categoryDTOList = response.body();
        if(categoryDTOList != null) {
          List<CategoryModel> list = CategoryMapper.map(categoryDTOList);
          listen.onSuccess(list);
        }


      }

      @Override
      public void onFailure(@NonNull Call<List<CategoryDTO>> call, @NonNull Throwable t) {

      }
    });
    return listen;
  }

  public void updateCategoryById(final CategoryDTO category, String id){
    apiCallCategory.updateCategory(id, category).enqueue(new Callback<CategoryDTO>() {
      @Override
      public void onResponse(Call<CategoryDTO> call, Response<CategoryDTO> response) {
        //TODO implementer listener
      }
      @Override
      public void onFailure(Call<CategoryDTO> call, Throwable t) {

        Log.e("OnFailureUptadeCategory", t.toString());
      }
    });

  }

  public void deleteCategory(final String idCategory){
    apiCallCategory.deleteCategory(idCategory).enqueue(new Callback<CategoryDTO>() {
      @Override
      public void onResponse(Call<CategoryDTO> call, Response<CategoryDTO> response) {



      }
      @Override
      public void onFailure(Call<CategoryDTO> call, Throwable t) {

        Log.e("ResponseDelete", t.toString());
      }
    });

  }


//User

  public Listen<JsonObject> addNewUser(UserDTO userDTO, final Listen<JsonObject> listen) {

    Call <JsonObject> call = apiCallUser.addNewUser(userDTO);
    call.enqueue(new Callback <JsonObject>() {
      @Override
      public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {

        JsonObject user = response.body();

        listen.onSuccess(user);

      }

      @Override
      public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {

      }
    });
    return listen;
  }

  public void getUserAccount(String idUser, final ListenUser<UserModel> listener) {
    apiCallUser.getUserById(idUser).enqueue(new Callback<UserDTO>() {
      @Override
      public void onResponse(@NonNull Call<UserDTO> call, @NonNull Response<UserDTO> response) {
        UserModel userModel = new UserModel();
        UserDTO userDTO = response.body();
        userModel = new UserMapper().map(userDTO);

        listener.onSuccess(userModel);
      }

      @Override
      public void onFailure(Call<UserDTO> call, Throwable t) {
        listener.onError(t);
      }
    });
  }



}
