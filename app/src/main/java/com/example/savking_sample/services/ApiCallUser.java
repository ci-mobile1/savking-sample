package com.example.savking_sample.services;

import com.example.savking_sample.DTO.UserDTO;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiCallUser {

    @POST("newUser")
    @Headers("Content-Type: application/json")
    Call<JsonObject> addNewUser (@Body UserDTO userDTO);


    @GET("getUserById/{userId}")
    Call<UserDTO> getUserById(@Path("userId") String _id);

    @PUT("updateUser/{id}")
    @Headers("Content-Type: application/json")
    Call<JsonObject> updateUserAccount (@Body UserDTO userDTO, @Path("userId") String idUser);
}
