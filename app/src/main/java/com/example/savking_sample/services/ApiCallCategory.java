package com.example.savking_sample.services;

import com.example.savking_sample.DTO.CategoryDTO;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiCallCategory {


    @POST("newCategory")
    @Headers("Content-Type: application/json")
    Call<JsonObject> addNewCategory (@Body CategoryDTO categoryDTO);

    @POST("getCategoriesByUser")
    @Headers("Content-Type: application/json")
    Call<List<CategoryDTO>> getCategoriesByUser (@Body JsonObject jsonObject);

    @PUT("updateCategoryById/{id}")
    Call<CategoryDTO> updateCategory(@Path("id") String id, @Body CategoryDTO categoryDTO);

    @DELETE("deleteCategoryById/{id}")
    Call<CategoryDTO> deleteCategory(@Path("id") String id);

}
