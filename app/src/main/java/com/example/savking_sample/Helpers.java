package com.example.savking_sample;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class Helpers {


    public void Alert (Context context, String title, String message, String btnTitle) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, btnTitle,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
        alertDialog.show();

    }
    public void GoTo (Fragment fragment, FragmentManager fragmentManager ) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.nav_host_fragment, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    public Date newDate(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.FRANCE);
        Date aDate = null;
        try {
            aDate = formatter.parse(date);
            return aDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Date convertLongToDate(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.FRANCE);
        String dateString = formatter.format(new Date((date * 1000)));
        Date theNewDate = newDate(dateString);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(theNewDate);
        Log.d("OKKKK", "convertLongToDate: " + theNewDate.toString());

        Log.d("TAG", "convertLongToDate:WITHOUT CHANGE " + calendar.getTime().toString());
        formatter.format(calendar.getTime());
        //calendar.set
        return theNewDate;

    }

    public String dateToString(Date date) {
        if(date == null){
            return "Indisponible";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.FRANCE);
        String dateToString = dateFormat.format(date);
        return dateToString;
    }

    public String dateToHour(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.FRANCE);

        String dateToHour = dateFormat.format(date);
        return dateToHour;
    }

    public String changeFormatDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);
        String dateToString = dateFormat.format(date);
        return dateToString;
    }

    public String dateLocalFrance(Date date) {
        if(date == null){
            return "Indisponible";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);
        return dateFormat.format(date);
    }

    public Date dateFormat(Date date) {
        String formatWanted = "dd/MM/yyyy";
        Date newDateFormat = date;
        SimpleDateFormat dateFormat = new SimpleDateFormat(formatWanted, Locale.FRANCE);
        try {
            newDateFormat = dateFormat.parse(date.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDateFormat;
    }
    public void showToast(String message, int timeShowed, Context context) {
        Toast toast = Toast.makeText(context, message, timeShowed);
        toast.show();
    }


}


