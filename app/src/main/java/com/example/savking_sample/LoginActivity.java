package com.example.savking_sample;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.savking_sample.services.NetworkProvider;
import com.example.savking_sample.signUp.SignUpActivity;
import com.google.gson.JsonObject;

public class LoginActivity extends AppCompatActivity {

    Button connexion;
    Button newUserBtn;
    Button iForgotMyPassword;
    ImageView img;
    EditText mail;
    EditText psswd;
    CheckBox chkConnect;

    public String jsonIdUser;
    private String jsonToken;
    String dateString;

    Helpers helpers = new Helpers();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        connexion = findViewById(R.id.login_connexion_btn);
        img = findViewById(R.id.img_logo_main);
        mail = findViewById(R.id.login_edt_pseudo);
        psswd = findViewById(R.id.login_edt_password);
        chkConnect = findViewById(R.id.chk_stay_connected);
        newUserBtn = findViewById(R.id.login_btn_new_user);
        iForgotMyPassword = findViewById(R.id.login_password_forgotten_btn);

        SharedPreferences preferencesLog = getSharedPreferences("TOKEN_ID", 0);

        jsonToken = preferencesLog.getString("token", "false");

        SharedPreferences preferences = getSharedPreferences("STAY_CONNECTED", 0);
        String connexionAuto = preferences.getString("connected", "false");

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(connexion.getWindowToken(), 0);

        if(connexionAuto.matches("true") && !jsonToken.equals("false")){
            //verifConnected(jsonToken);
        }

        newUserBtn.setOnClickListener(view -> {
           // iForgotMyPassword.setVisibility(View.INVISIBLE);
            Intent intent = new Intent(getBaseContext(), SignUpActivity.class);
            startActivity(intent);

        });

        connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!psswd.getText().toString().trim().isEmpty() && !mail.getText().toString().trim().isEmpty()) {
                    login();
                    if(chkConnect.isChecked()){
                        SharedPreferences.Editor editor = getSharedPreferences("STAY_CONNECTED", MODE_PRIVATE).edit();
                        editor.putString("connected", "true");
                        editor.apply();
                    }else {
                        SharedPreferences.Editor editor = getSharedPreferences("STAY_CONNECTED", MODE_PRIVATE).edit();
                        editor.putString("connected", "false");
                        editor.apply();
                    }
                } else if (mail.getText().toString().trim().isEmpty()) {
                    helpers.showToast(getResources().getString(R.string.toast_empty_pseudo), Toast.LENGTH_SHORT, getApplicationContext());

                }else {
                    helpers.showToast(getResources().getString(R.string.toast_empty_password), Toast.LENGTH_SHORT, getApplicationContext());
                }

            }

        });
        iForgotMyPassword.setOnClickListener(v-> {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("\uD83D\uDE2D\uD83D\uDE2D\uD83D\uDE2D Mot de Passe oublié \uD83D\uDE2D\uD83D\uDE2D\uD83D\uDE2D ");
            alertDialog.setMessage("Cette fonctionnalité est malheureusement indisponible pour le moment \uD83D\uDE35\uD83D\uDE36\uD83D\uDE31\n" +
                    "Comment faire \uD83D\uDE35⁉️ \uD83D\uDE29\uD83D\uDE1E\uD83D\uDE23\uD83D\uDE16\n" +
                    "Bonne recherche \uD83E\uDDD0\uD83E\uDDD0\uD83E\uDDD0");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Fermer",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            Toast toast = Toast.makeText(this, "En cours de développement" , Toast.LENGTH_LONG);
            toast.show();
        });

    }


    private void login() {
        String email = mail.getText().toString();
        String pass = psswd.getText().toString();

        final NetworkProvider.Listen<JsonObject> object = NetworkProvider.getInstance().login(email, pass, new NetworkProvider.Listen<JsonObject>() {

            @Override
            public void onSuccess(JsonObject data) {


                if(data != null){
                    jsonIdUser = String.valueOf(data.get("idUser"));
                    jsonToken = String.valueOf(data.get("token"));


                    if(jsonIdUser != null) {

                        String jsonPseudo = String.valueOf(data.get("pseudo"));
                        dateString = String.valueOf(data.get("date"));


                        SharedPreferences.Editor editor = getSharedPreferences("TOKEN_ID", MODE_PRIVATE).edit();
                        String token = jsonToken.replace("\"", "");
                        String idUser = jsonIdUser.replace("\"", "");
                        String pseudo = jsonPseudo.replace("\"", "");
                        //Log.d("TAG", "onSuccess: "+dateString.toString());

                        int month = data.get("month").getAsInt();
                        int year = data.get("year").getAsInt();

                        String dateParsed = dateString.replace("\"", "");
                        //Log.d("TAG", "onSuccess: "+ dateParsed.toString() );


                        //Date date = helpers.newDate(dateParsed);


                        //long createdUserAccount = date.getTime();

                        editor.putString("token", token);
                        editor.putString("idUser", idUser);
                        editor.putString("pseudo", pseudo);
                        editor.putInt("year", year);
                        editor.putInt("month", month);

                        editor.apply();
                    }
                    if(jsonIdUser != null && !jsonToken.equals("")){
                        Toast toast = Toast.makeText(getApplicationContext(), "Connecté", Toast.LENGTH_SHORT);
                        toast.show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }

                }else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Serveur en Maintenance, réessayer ultérieurement", Toast.LENGTH_SHORT);
                    toast.show();
                }

            }

            @Override
            public void onError(Throwable t) {
                Toast toast = Toast.makeText(getApplicationContext(), "Non connecté à Internet", Toast.LENGTH_SHORT);
                toast.show();
            }

        });
    }

    private void verifConnected(String token) {
        final NetworkProvider.Listen<JsonObject> object = NetworkProvider.getInstance().verifConnected(token, new NetworkProvider.Listen<JsonObject>() {

            @Override
            public void onSuccess(JsonObject data) {
                if(data != null){
                    String checking = String.valueOf(data.get("auth"));
                    if(checking.matches("true")){
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }else {
                        SharedPreferences.Editor editor = getSharedPreferences("STAY_CONNECTED", MODE_PRIVATE).edit();
                        editor.putString("connected", "false");
                        editor.apply();
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                Log.d("TOKTOK_ERROR", t.toString());
            }

        });
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(R.string.leave)
                .setPositiveButton(R.string.yes, (dialog, id) -> {
                    finish();
                })
                .setNegativeButton(R.string.no, (dialog, id) -> {
                });
        builder.show();

    }

    private void switchOn(String token) {
        final NetworkProvider.Listen<JsonObject> object = NetworkProvider.getInstance().verifConnected(token, new NetworkProvider.Listen<JsonObject>() {

            @Override
            public void onSuccess(JsonObject data) {

            }

            @Override
            public void onError(Throwable t) {
                Log.d("TOKTOK_ERROR", t.toString());
            }

        });
    }

}