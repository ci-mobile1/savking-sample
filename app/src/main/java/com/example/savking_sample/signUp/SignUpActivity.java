package com.example.savking_sample.signUp;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.savking_sample.DTO.UserDTO;
import com.example.savking_sample.LoginActivity;
import com.example.savking_sample.R;
import com.example.savking_sample.services.NetworkProvider;
import com.google.gson.JsonObject;

public class SignUpActivity extends AppCompatActivity {

    EditText emailEdt;
    EditText passwordEdt;
    EditText password2Edt;
    EditText pseudoEdt;

    Button saveBtn;
    boolean buttonPushed = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);


        saveBtn = findViewById(R.id.signup_create);
        emailEdt = findViewById(R.id.signup_email_edt);
        passwordEdt = findViewById(R.id.signup_password_edt);
        password2Edt = findViewById(R.id.signup_password_second_edt);
        pseudoEdt = findViewById(R.id.signup_pseudo_edt);


        saveBtn.setOnClickListener(v ->{
            if (!emailEdt.getText().toString().trim().matches("") && !buttonPushed) {
                if (!pseudoEdt.getText().toString().trim().matches("")) {
                    if(passwordEdt.getText().toString().matches(password2Edt.getText().toString()) && !passwordEdt.getText().toString().isEmpty()) {
                        boolean pseudo = empty(pseudoEdt);
                        boolean emailBool = empty(emailEdt);

                        if(!pseudo && !emailBool) {
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);

                            UserDTO userDTO = new UserDTO();
                            buttonPushed = true;

                            String email = emailEdt.getText().toString();
                            String password = passwordEdt.getText().toString();
                            String psudo = pseudoEdt.getText().toString();
                            userDTO.setEmail(email);
                            userDTO.setuPassword(password);
                            userDTO.setuPseudo(psudo);
                            userDTO.setuAccessType("user");
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            NetworkProvider.getInstance().addNewUser(userDTO, new NetworkProvider.Listen<JsonObject>() {

                                @Override
                                public void onSuccess(JsonObject data) {
                                    if(data != null){
                                        if(!String.valueOf(data.get("_id")).matches("")){
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                        }
                                    }
                                }

                                @Override
                                public void onError(Throwable t) {

                                }
                            });


                        }else if (pseudo) {
                            toast("Pseudo");
                        }else if (emailBool) {
                            toast("Email");
                        }else {
                            toast("profil");
                        }
                    } else {
                        Toast toast = Toast.makeText(getApplicationContext(), "Les mots de passe ne concorde pas", Toast.LENGTH_LONG);
                        toast.show();
                    }

                } else {
                    Toast toast = Toast.makeText(this, "Pseudo requis", Toast.LENGTH_LONG);
                    toast.show();
                }

            } else {
                Toast toast = Toast.makeText(this, "Champs adresse email vide", Toast.LENGTH_LONG);
                toast.show();
            }
        } );


    }



    private boolean empty(EditText editText) {
        return editText.getText().toString().isEmpty();
    }
    private void toast(String name){
        Toast toast = Toast.makeText(getApplicationContext(), "Merci de renseigner un " + name, Toast.LENGTH_LONG);
        toast.show();
    }

}