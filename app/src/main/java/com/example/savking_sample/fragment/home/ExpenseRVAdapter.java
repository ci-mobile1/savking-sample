package com.example.savking_sample.fragment.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.savking_sample.Helpers;
import com.example.savking_sample.models.ExpenseProgramModel;
import com.example.savking_sample.R;
import com.google.android.material.button.MaterialButton;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExpenseRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    public List<ExpenseProgramModel> expenseProgramList;
    public List<ExpenseProgramModel> dataFull = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private ButtonClickListener buttonClickListener;
    private int countChar = 0;
    int type;

    public ExpenseRVAdapter (int typ) {
        type = typ;
    }

    public void setDataCash(List<ExpenseProgramModel> expenseProgramModels) {
        if(expenseProgramModels != null){
            this.expenseProgramList = expenseProgramModels;
            this.dataFull.addAll(expenseProgramModels);
            notifyDataSetChanged();
        }

    }

    public void setButtonClickListener(ButtonClickListener buttonClickListener) {
        this.buttonClickListener = buttonClickListener;
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        if(viewType == 0) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_card_view_row, viewGroup, false);
            return new com.example.savking_sample.fragment.home.ExpenseRVAdapter.ExpenseBlockViewHolder(view);

            //View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_recycler_row, viewGroup, false);
            //return new RecyclerViewAdapter.CashViewHolder(view);
        }
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_expense_block_view_holder, viewGroup, false);
        return new ExpenseBlockViewHolder(view);



    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);
        final ExpenseProgramModel expenseProgramModel = expenseProgramList.get(position);

            NumberFormat nf = new DecimalFormat("0.##");
            nf.setMinimumFractionDigits(2);
            String valueAmount = nf.format(expenseProgramModel.geteCreditCard());

            com.example.savking_sample.fragment.home.ExpenseRVAdapter.ExpenseBlockViewHolder expenseBlockViewHolder = (com.example.savking_sample.fragment.home.ExpenseRVAdapter.ExpenseBlockViewHolder) holder;
            expenseBlockViewHolder.title_block.setText(expenseProgramModel.geteTitle());
            expenseBlockViewHolder.amount_block.setText(valueAmount + " €");
            expenseBlockViewHolder.category.setText(expenseProgramModel.geteCategory());

            if(expenseProgramModel.geteDateBegin() != null) {
                Date creationDate  = expenseProgramModel.geteDateBegin();
                Helpers helpers = new Helpers();
                String createString = helpers.dateToString(creationDate);
                ((com.example.savking_sample.fragment.home.ExpenseRVAdapter.ExpenseBlockViewHolder) holder).date_block.setText(createString);
            } else {
                ((com.example.savking_sample.fragment.home.ExpenseRVAdapter.ExpenseBlockViewHolder) holder).date_block.setText(R.string.not_available);
            }





        //Glide.with(weaponViewHolder.itemView).load(weapon.getPictureUrl()).into(weaponViewHolder.pictureImv);

    }


    @Override
    public int getItemCount() {
        return expenseProgramList != null ? expenseProgramList.size() : 0;
    }

    class ExpenseViewHolder extends RecyclerView.ViewHolder  {

        TextView card_category;
        TextView card_title;
        TextView card_amount;
        TextView card_description;
        Button card_editBtn;
        Button card_deleteBtn;
        MaterialButton scrollDown;


        ExpenseViewHolder(View itemView) {
            super(itemView);
            this.itemView.findViewById(itemView.getId());

            card_title = itemView.findViewById(R.id.card_view_title);
            card_amount = itemView.findViewById(R.id.card_view_amount);
            card_deleteBtn = itemView.findViewById(R.id.card_view_delete_btn);
            card_editBtn = itemView.findViewById(R.id.card_view_edit_btn);
            card_description = itemView.findViewById(R.id.card_view_description);
            scrollDown = itemView.findViewById(R.id.card_view_scroll_btn);
            card_category = itemView.findViewById(R.id.card_view_category);
/*
            recycler_title = itemView.findViewById(R.id.recycler_expense_name);
            recycler_amount = itemView.findViewById(R.id.recycler_amount);
            recycler_category = itemView.findViewById(R.id.recycler_category_name);
            parent_layout = itemView.findViewById(R.id.parent_layout_recycler);
            recycler_logo_row = itemView.findViewById(R.id.recycler_logo_row);
*/
        }


    }

    class ExpenseBlockViewHolder extends RecyclerView.ViewHolder  {
        ImageView recycler_logo_row;
        TextView date_block;
        TextView title_block;
        TextView amount_block;
        TextView category;


        ExpenseBlockViewHolder(View itemView) {
            super(itemView);
            this.itemView.findViewById(itemView.getId());

            title_block = itemView.findViewById(R.id.block_title_txt);
            date_block = itemView.findViewById(R.id.block_date_txt);
            amount_block = itemView.findViewById(R.id.block_amount_txt);
            category = itemView.findViewById(R.id.block_category_txt);

        }


    }

    @Override
    public int getItemViewType(int position) {
        if(type == 1 ) {
            return 1;
        }
        return 0;
    }

    public interface ItemClickListener {
        void onClick(ExpenseProgramModel expenseProgramModel);
    }
    public interface ButtonClickListener {
        void onButtonClick(ExpenseProgramModel expenseProgramModel, int position);
    }


}

