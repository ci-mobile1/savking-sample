package com.example.savking_sample.fragment.cashboard.expense;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import com.example.savking_sample.FragmentControl;
import com.example.savking_sample.Helpers;
import com.example.savking_sample.R;
import com.example.savking_sample.fragment.cashboard.CashboardFragment;
import com.example.savking_sample.services.NetworkProvider;

import java.util.Objects;


public class DetailCashFragment extends Fragment {

    TextView titleTxt;
    TextView descriptionTxt;
    TextView cbTxt;
    TextView trTxt;
    TextView greenCashTxt;
    TextView dateTxt;
    TextView categoryTxt;

    String title = null;
    String description = null;
    String createAt = null;
    String id = null;
    String category;
    double cb = 0.0;
    double tr = 0.0;
    double greenCash = 0.0;

    public DetailCashFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_detail_cash, container, false);
        Objects.requireNonNull(((AppCompatActivity) requireActivity()).getSupportActionBar()).setTitle("Sav[K]ing - Information");
        titleTxt = root.findViewById(R.id.detail_cash_title);
        descriptionTxt = root.findViewById(R.id.detail_cash_description);
        cbTxt = root.findViewById(R.id.detail_cash_cb);
        trTxt = root.findViewById(R.id.detail_cash_tr);
        greenCashTxt = root.findViewById(R.id.detail_cash_greencash);
        dateTxt = root.findViewById(R.id.detail_cash_date);
        categoryTxt = root.findViewById(R.id.detail_cash_category_txt);

        if (getArguments() != null) {
            title = getArguments().getString("title", "Indisponible");
            id = getArguments().getString("id", "Non Disponible");
            tr = getArguments().getDouble("tr", 0.0);
            cb = getArguments().getDouble("cb", 0.0);
            greenCash = getArguments().getDouble("greenCash", 0.0);

            titleTxt.setText(title);

            String cbToString = Double.toString(cb);
            String trToString = Double.toString(tr);
            String greenCashToString = Double.toString(greenCash);

            cbTxt.setText(cbToString);
            trTxt.setText(trToString);
            greenCashTxt.setText(greenCashToString);

            description = getArguments().getString("description");
            descriptionTxt.setText(description);
            category = getArguments().getString("category");
            createAt = getArguments().getString("createAt");
            dateTxt.setText(createAt);
            categoryTxt.setText(category);


        }



        return root;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_menu_modify, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_delete:
                AlertDialog.Builder build = new AlertDialog.Builder(getContext());

                build.setMessage("Supprimer cette dépense ?")
                        .setPositiveButton(R.string.yes, (dialog, idExpense) -> {

                            NetworkProvider.getInstance().deleteCash(id);

                            FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                            FragmentControl.fragmentTransaction(CashboardFragment.newInstance(), fragmentManager,null, null);

                        })
                        .setNegativeButton(R.string.no, (dialog, id) -> {
                        });
                build.show();
                break;
            case R.id.action_modify:


                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                builder.setMessage("Mofifier cette dépense ?")
                        .setPositiveButton(R.string.yes, (dialog, idExpense) -> {


                        })
                        .setNegativeButton(R.string.no, (dialog, id) -> {
                        });
                builder.show();


                break;
        }

        return super.onOptionsItemSelected(item);
    }
}