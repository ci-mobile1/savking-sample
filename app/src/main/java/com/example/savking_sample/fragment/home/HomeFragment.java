package com.example.savking_sample.fragment.home;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.savking_sample.models.CategoryModel;
import com.example.savking_sample.models.ExpenseProgramModel;
import com.example.savking_sample.models.IncomeProgramModel;
import com.example.savking_sample.R;
import com.example.savking_sample.services.NetworkProvider;
import com.google.gson.JsonObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Objects;

public class HomeFragment extends Fragment {


    private String idUser;
    private String token;

    private RecyclerView expenseRecyclerView;
    private RecyclerView incomeRecyclerView;

    List<CategoryModel> categoryModelList;

    private ExpenseRVAdapter expenseRecyclerViewAdapter;
    private IncomeRVAdapter incomeRecyclerViewAdapter;

    TextView cbTxt;
    TextView trTxt;
    TextView espTxt;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        expenseRecyclerView = root.findViewById(R.id.home_recycler_view_expense);
        incomeRecyclerView = root.findViewById(R.id.home_recycler_view_income);
        TextView homeWelcome = root.findViewById(R.id.home_message);
        cbTxt = root.findViewById(R.id.home_cb);
        trTxt = root.findViewById(R.id.home_tr);
        espTxt = root.findViewById(R.id.home_esp);

        Objects.requireNonNull(((AppCompatActivity) requireActivity()).getSupportActionBar()).setTitle("Sav[K]ing ");

        SharedPreferences preferences = requireActivity().getSharedPreferences("TOKEN_ID", 0);
        idUser = preferences.getString("idUser", "id");
        token = preferences.getString("token", "token");
        String pseudo = preferences.getString("pseudo", "pseudo");
        homeWelcome.setText("Bonjour " + pseudo);

        getMyWallet();
        //loadCategories();

        initRecyclerViewExpense();
        initRecyclerViewIncome();

        loadDataActualMonth();
        loadIncomeActualMonth();
        // loadCategories();

        /*homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        */
        //loading();

        return root;
    }

    private void getMyWallet() {

        NetworkProvider.getInstance().getMoneyLeft(token, idUser, new NetworkProvider.Listen<JsonObject>() {
            @Override
            public void onSuccess(JsonObject data) {
                if (data != null) {
                    if (data.get("cb") != null) {
                        double cb = Double.parseDouble(String.valueOf(data.get("cb")));
                        NumberFormat format = new DecimalFormat("0.##");
                        format.setMinimumFractionDigits(2);
                        String cbText = format.format(cb);
                        cbTxt.setText(cbText + " €");
                        cbTxt.setTextColor(0xFF2196F3);
                    }
                    if (data.get("tr") != null) {
                        double tr = Double.parseDouble(String.valueOf(data.get("tr")));
                        NumberFormat format = new DecimalFormat("0.##");
                        format.setMinimumFractionDigits(2);
                        String trText = format.format(tr);
                        trTxt.setText(trText + " €");
                        cbTxt.setTextColor(0xFF2196F3);
                    }
                    if (data.get("esp") != null) {
                        double esp = Double.parseDouble(String.valueOf(data.get("esp")));
                        NumberFormat format = new DecimalFormat("0.##");
                        format.setMinimumFractionDigits(2);
                        String espText = format.format(esp);
                        espTxt.setText(espText + " €");
                        espTxt.setTextColor(0xFF2196F3);
                    }

                }

            }

            @Override
            public void onError(Throwable t) {
                Log.d("TAG", "onERRRR: " + t);

            }
        });

    }


    private void initRecyclerViewExpense() {

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        expenseRecyclerView.setLayoutManager(layoutManager);
        expenseRecyclerViewAdapter = new ExpenseRVAdapter(1);
        expenseRecyclerView.setAdapter(expenseRecyclerViewAdapter);

        expenseRecyclerViewAdapter.setItemClickListener(expenseModel -> {

        });
    }

    private void initRecyclerViewIncome() {

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        incomeRecyclerView.setLayoutManager(layoutManager);
        incomeRecyclerViewAdapter = new IncomeRVAdapter(1);
        incomeRecyclerView.setAdapter(incomeRecyclerViewAdapter);

        incomeRecyclerViewAdapter.setItemClickListener(incomeModel -> {

        });
    }

    private void loadDataActualMonth() {
        NetworkProvider.getInstance().getExpenseProgramOnActualMonth(token, new NetworkProvider.Listener<ExpenseProgramModel>() {
            @Override
            public void onSuccess(List<ExpenseProgramModel> data) {
                if (data != null) {
                    expenseRecyclerViewAdapter.setDataCash(data);
                }
            }

            @Override
            public void onError(Throwable t) {

            }
        });

    }

    private void loadIncomeActualMonth() {
        SharedPreferences preferences = requireActivity().getSharedPreferences("TOKEN_ID", 0);
        token = preferences.getString("token", "token");

        NetworkProvider.getInstance().getIncomeProgramOnActualMonth(token, new NetworkProvider.Listener<IncomeProgramModel>() {
            @Override
            public void onSuccess(List<IncomeProgramModel> data) {
                if (data != null) {
                    incomeRecyclerViewAdapter.setIncomeProgData(data);
                }
            }

            @Override
            public void onError(Throwable t) {

            }

        });

    }
}