package com.example.savking_sample.fragment.cashboard;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CashboardViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public CashboardViewModel() {
        mText = new MutableLiveData<>();
    }

    public LiveData<String> getText() {
        return mText;
    }
}