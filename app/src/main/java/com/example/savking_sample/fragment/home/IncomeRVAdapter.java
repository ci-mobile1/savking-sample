package com.example.savking_sample.fragment.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.savking_sample.Helpers;
import com.example.savking_sample.models.ExpenseProgramModel;
import com.example.savking_sample.models.IncomeProgramModel;
import com.example.savking_sample.R;
import com.google.android.material.button.MaterialButton;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class IncomeRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    public List<IncomeProgramModel> incomeProgramModels;
    public List<IncomeProgramModel> dataFull = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private ButtonClickListener buttonClickListener;
    private int countChar = 0;
    int type;

    public IncomeRVAdapter (int typ) {
        type = typ;
    }

    public void setIncomeProgData(List<IncomeProgramModel> incomeProgData) {
        if(incomeProgData != null){
            this.incomeProgramModels = incomeProgData;
            this.dataFull.addAll(incomeProgData);
            notifyDataSetChanged();
        }

    }

    public void setButtonClickListener(ButtonClickListener buttonClickListener) {
        this.buttonClickListener = buttonClickListener;
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        if(viewType == 0) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_card_view_row, viewGroup, false);
            return new com.example.savking_sample.fragment.home.IncomeRVAdapter.IncomeBlockViewHolder(view);

            //View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_recycler_row, viewGroup, false);
            //return new RecyclerViewAdapter.CashViewHolder(view);
        }
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_expense_block_view_holder, viewGroup, false);
        return new IncomeBlockViewHolder(view);



    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);
        double total = 0.0;
        final IncomeProgramModel incomeProgramModel = incomeProgramModels.get(position);
        if(type == 0) {
            /*
            com.example.my_app_20.fragment.cashboard.expense.RecyclerViewAdapter.CashViewHolder cashViewHolder = (com.example.my_app_20.fragment.cashboard.expense.RecyclerViewAdapter.CashViewHolder) holder;
            cashViewHolder.card_title.setText(cash.getTitle());
            NumberFormat nf = new DecimalFormat("0.##");
            nf.setMinimumFractionDigits(2);
            String valueAmount = nf.format(cash.getCreditCard() + cash.getGreenCash() + cash.getcTR());

            cashViewHolder.card_amount.setText(valueAmount);
            if(cash.isSelected()) {
                cashViewHolder.card_category.setVisibility(View.VISIBLE);
                cashViewHolder.card_category.setText(cash.getCategory());

                cashViewHolder.card_title.setVisibility(View.VISIBLE);
                cashViewHolder.card_description.setText(cash.getDescription());
                cashViewHolder.card_description.setVisibility(View.VISIBLE);
                cashViewHolder.card_editBtn.setVisibility(View.VISIBLE);
                cashViewHolder.card_deleteBtn.setVisibility(View.VISIBLE);
                cashViewHolder.scrollDown.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_arrow_drop_up_24, 0, 0, 0);



            }else {
                expenseViewHolder.card_category.setVisibility(View.INVISIBLE);

                cashViewHolder.card_description.setVisibility(View.GONE);
                cashViewHolder.card_editBtn.setVisibility(View.GONE);
                cashViewHolder.card_deleteBtn.setVisibility(View.GONE);
                cashViewHolder.scrollDown.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_arrow_drop_down_24, 0, 0, 0);

            }*/
/*
            if (itemClickListener != null) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemClickListener.onClick(cash);
                    }
                });

            }
            if (buttonClickListener != null) {
                ((com.example.my_app_20.fragment.cashboard.expense.RecyclerViewAdapter.CashViewHolder) holder).scrollDown.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        buttonClickListener.onButtonClick(cash,position);
                    }
                });

            }
*/

        } else {
            NumberFormat nf = new DecimalFormat("0.##");
            nf.setMinimumFractionDigits(2);
            String valueAmount = nf.format(incomeProgramModel.getiCreditCard());

            com.example.savking_sample.fragment.home.IncomeRVAdapter.IncomeBlockViewHolder incomeBlockViewHolder = (com.example.savking_sample.fragment.home.IncomeRVAdapter.IncomeBlockViewHolder) holder;
            incomeBlockViewHolder.title_block.setText(incomeProgramModel.getiTitle());
            incomeBlockViewHolder.amount_block.setText(valueAmount + " €");
            incomeBlockViewHolder.category.setText(incomeProgramModel.getiCategory());

            if(incomeProgramModel.getiDateBegin() != null) {
                Date creationDate  = incomeProgramModel.getiDateBegin();
                Helpers helpers = new Helpers();
                String createString = helpers.dateToString(creationDate);
                ((com.example.savking_sample.fragment.home.IncomeRVAdapter.IncomeBlockViewHolder) holder).date_block.setText(createString);
            } else {
                ((com.example.savking_sample.fragment.home.IncomeRVAdapter.IncomeBlockViewHolder) holder).date_block.setText(R.string.not_available);
            }
        }




        //Glide.with(weaponViewHolder.itemView).load(weapon.getPictureUrl()).into(weaponViewHolder.pictureImv);

    }


    @Override
    public int getItemCount() {
        return incomeProgramModels != null ? incomeProgramModels.size() : 0;
    }

    class ExpenseViewHolder extends RecyclerView.ViewHolder  {

        TextView card_category;
        TextView card_title;
        TextView card_amount;
        TextView card_description;
        Button card_editBtn;
        Button card_deleteBtn;
        MaterialButton scrollDown;


        ExpenseViewHolder(View itemView) {
            super(itemView);
            this.itemView.findViewById(itemView.getId());

            card_title = itemView.findViewById(R.id.card_view_title);
            card_amount = itemView.findViewById(R.id.card_view_amount);
            card_deleteBtn = itemView.findViewById(R.id.card_view_delete_btn);
            card_editBtn = itemView.findViewById(R.id.card_view_edit_btn);
            card_description = itemView.findViewById(R.id.card_view_description);
            scrollDown = itemView.findViewById(R.id.card_view_scroll_btn);
            card_category = itemView.findViewById(R.id.card_view_category);
/*
            recycler_title = itemView.findViewById(R.id.recycler_expense_name);
            recycler_amount = itemView.findViewById(R.id.recycler_amount);
            recycler_category = itemView.findViewById(R.id.recycler_category_name);
            parent_layout = itemView.findViewById(R.id.parent_layout_recycler);
            recycler_logo_row = itemView.findViewById(R.id.recycler_logo_row);
*/
        }


    }

    class IncomeBlockViewHolder extends RecyclerView.ViewHolder  {
        ImageView recycler_logo_row;
        TextView date_block;
        TextView title_block;
        TextView amount_block;
        TextView category;


        IncomeBlockViewHolder(View itemView) {
            super(itemView);
            this.itemView.findViewById(itemView.getId());

            title_block = itemView.findViewById(R.id.block_title_txt);
            date_block = itemView.findViewById(R.id.block_date_txt);
            amount_block = itemView.findViewById(R.id.block_amount_txt);
            category = itemView.findViewById(R.id.block_category_txt);

        }


    }

    @Override
    public int getItemViewType(int position) {
        if(type == 1 ) {
            return 1;
        }
        return 0;
    }

    public interface ItemClickListener {
        void onClick(ExpenseProgramModel expenseProgramModel);
    }
    public interface ButtonClickListener {
        void onButtonClick(ExpenseProgramModel expenseProgramModel, int position);
    }


}


