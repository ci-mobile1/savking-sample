package com.example.savking_sample.fragment.cashboard.expense;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.savking_sample.DTO.CashDTO;
import com.example.savking_sample.FragmentControl;
import com.example.savking_sample.models.CategoryModel;
import com.example.savking_sample.R;
import com.example.savking_sample.fragment.cashboard.CashboardFragment;
import com.example.savking_sample.services.NetworkProvider;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class AddExpenseFragment extends Fragment {

    TextInputLayout titleEdt;
    TextInputLayout amountCbEdt;
    TextInputLayout amountTrEdt;
    TextInputLayout amountGreenCashEdt;
    TextInputLayout descriptionEdt;

    MaterialCheckBox cbChk;
    MaterialCheckBox trChk;
    MaterialCheckBox espChk;

    Button send;
    AutoCompleteTextView editTextFilledExposedDropdown;

    ArrayAdapter<String> adapterCategory;

    private String categoryChosen;
    private String idCategorySelected;
    private String token;
    private static String idUser;
    String defaultIdCategoryList;

    List<CategoryModel> categoryModelList = new ArrayList<>();
    List<String> defaultCategoryNameList = new ArrayList<>();


    NumberFormat nf = new DecimalFormat("0.##");


    public AddExpenseFragment() {
    }


    public static com.example.savking_sample.fragment.cashboard.expense.AddExpenseFragment newInstance(String userId) {
        idUser = userId;
        com.example.savking_sample.fragment.cashboard.expense.AddExpenseFragment fragment = new com.example.savking_sample.fragment.cashboard.expense.AddExpenseFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_expense, container, false);
        defaultIdCategoryList = getString(R.string.defaultUser);
        send = root.findViewById(R.id.add_cash_save_btn);
        titleEdt = root.findViewById(R.id.add_cash_title_edt);
        descriptionEdt = root.findViewById(R.id.add_cash_description_edt);
        amountGreenCashEdt = root.findViewById(R.id.add_cash_green_cash_edt);
        amountCbEdt = root.findViewById(R.id.add_cash_cb_edt);
        amountTrEdt = root.findViewById(R.id.add_cash_tr_edt);
        editTextFilledExposedDropdown = root.findViewById(R.id.add_cash_category_auto_txt);

        cbChk = root.findViewById(R.id.add_cash_chk_cb);
        trChk = root.findViewById(R.id.add_cash_chk_tr);
        espChk = root.findViewById(R.id.add_cash_chk_green_cash);

        SharedPreferences preferences = requireActivity().getSharedPreferences("TOKEN_ID", 0);
        token = preferences.getString("token", "token");
        idUser = preferences.getString("idUser", "id");
        Objects.requireNonNull(((AppCompatActivity) requireActivity()).getSupportActionBar()).setTitle("Ajouter une dépense");


        String selectACategory = "Séléctionnez une catégorie";
        defaultCategoryNameList.add(selectACategory);

        adapterCategory =
                new ArrayAdapter<>(
                        getContext(),
                        R.layout.category_textview_layout,
                        defaultCategoryNameList);


        editTextFilledExposedDropdown.setAdapter(adapterCategory);

        loadCategories(defaultIdCategoryList);
        loadCategories(idUser);

        send.addTextChangedListener(addTextWatcher);

        cbChk.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                amountCbEdt.setVisibility(View.VISIBLE);
            }else {
                amountCbEdt.setVisibility(View.GONE);
            }
        });

        trChk.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                amountTrEdt.setVisibility(View.VISIBLE);
            }else {
                amountTrEdt.setVisibility(View.GONE);
            }
        });

        espChk.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                amountGreenCashEdt.setVisibility(View.VISIBLE);
            }else {
                amountGreenCashEdt.setVisibility(View.GONE);
            }
        });

        send.setOnClickListener( s -> {
            getCategoryChosen();

            if(titleEdt.getEditText().getText().toString().isEmpty()){
                Toast toast = Toast.makeText(getContext(), "Merci de renseigner un Titre", Toast.LENGTH_LONG);
                toast.show();
            } else if (categoryChosen.matches(selectACategory)) {
                Toast toast = Toast.makeText(getContext(), "Séléctionnez une Catégorie", Toast.LENGTH_LONG);
                toast.show();
            }else {

                CashDTO cashDTO = new CashDTO();

                String titleName = titleEdt.getEditText().getText().toString();
                cashDTO.setTitle(titleName);

                if (!descriptionEdt.getEditText().getText().toString().isEmpty()) {
                    cashDTO.setDescription(descriptionEdt.getEditText().getText().toString());
                }

                if (!amountCbEdt.getEditText().getText().toString().trim().isEmpty()) {
                    cashDTO.setCreditCard(Double.parseDouble(amountCbEdt.getEditText().getText().toString()));
                }

                if (!amountGreenCashEdt.getEditText().getText().toString().trim().isEmpty()) {
                    cashDTO.setGreenCash(Double.parseDouble(amountGreenCashEdt.getEditText().getText().toString()));
                }

                if (!amountTrEdt.getEditText().getText().toString().trim().isEmpty()) {
                    cashDTO.setcTR(Double.parseDouble(amountTrEdt.getEditText().getText().toString()));
                }

                String idCategory = getIdCategorySelected(categoryChosen);
                cashDTO.setCategory(idCategory);
                cashDTO.setIdUser(idUser);



                NetworkProvider.getInstance().addNewCash(cashDTO);

                Toast toast = Toast.makeText(getContext(), "Dépense ajouté!", Toast.LENGTH_LONG);
                toast.show();

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentControl.fragmentTransactionPop(CashboardFragment.newInstance(), fragmentManager, null );
            }
        });


        return root;
    }

    private void loadCategories (String idUserCategoryOwner) {

        NetworkProvider.getInstance().getCategoriesByUser(idUserCategoryOwner, token, 0,new NetworkProvider.ListenerCategory<CategoryModel>() {
            @Override
            public void onSuccess(List<CategoryModel> data) {
                if(data != null ) {
                    categoryModelList.addAll(data);
                }
                for(CategoryModel categoryModel : data) {
                    defaultCategoryNameList.add(categoryModel.getCategoryName());
                }

                if(idUserCategoryOwner.matches(idUser)) {

                    adapterCategory =
                            new ArrayAdapter<>(
                                    getContext(),
                                    R.layout.category_textview_layout,
                                    defaultCategoryNameList);

                    editTextFilledExposedDropdown.setAdapter(adapterCategory);

                }

            }

            @Override
            public void onError(Throwable t) {
                Log.d(("categoryAct"), "onError: " + t);
            }
        });
    }

    private void getCategoryChosen() {
        if (!editTextFilledExposedDropdown.getText().toString().matches("Catégorie")) {
            categoryChosen = editTextFilledExposedDropdown.getText().toString();
            idCategorySelected = getIdCategorySelected(categoryChosen);
        }
    }



    private String getIdCategorySelected(String categorySelected) {
        for(CategoryModel categoryModel : categoryModelList) {
            if(categoryModel.getCategoryName().matches(categorySelected)) {
                return categoryModel.getId();
            }
        }
        return "";
    }


    private TextWatcher addTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String validation = titleEdt.getEditText().getText().toString().trim();
            send.setEnabled(!validation.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
}