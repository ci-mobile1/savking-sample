package com.example.savking_sample.fragment.cashboard.income;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import com.example.savking_sample.DTO.IncomeDTO;
import com.example.savking_sample.R;
import com.example.savking_sample.services.NetworkProvider;

import java.util.Objects;


public class ModifyIncomeFragment extends Fragment {

    EditText titleEdt;
    EditText descriptionEdt;
    EditText cbEdt;
    EditText trEdt;
    EditText greenCashEdt;
    TextView dateTxt;

    String title = null;
    String description = null;
    String createAt = null;
    String id = null;

    double cb = 0.0;
    double tr = 0.0;
    double greenCash = 0.0;


    public ModifyIncomeFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    public static ModifyIncomeFragment newInstance() {
        ModifyIncomeFragment fragment = new ModifyIncomeFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_modify_income, container, false);

        Objects.requireNonNull(((AppCompatActivity) requireActivity()).getSupportActionBar()).setTitle("Sav[K]ing - Modifier la dépense");
        titleEdt = root.findViewById(R.id.modify_income_title);
        descriptionEdt = root.findViewById(R.id.modify_income_description);
        cbEdt = root.findViewById(R.id.modify_income_cb);
        trEdt = root.findViewById(R.id.modify_income_tr);
        greenCashEdt = root.findViewById(R.id.modify_income_greencash);
        dateTxt = root.findViewById(R.id.modify_income_date);
        if (getArguments() != null) {
            title = getArguments().getString("title", "Indisponible");
            id = getArguments().getString("id", "Non Disponible");
            tr = getArguments().getDouble("tr", 0.0);
            cb = getArguments().getDouble("cb", 0.0);
            greenCash = getArguments().getDouble("greenCash", 0.0);

            titleEdt.setText(title);

            String cbToString = Double.toString(cb);
            String trToString = Double.toString(tr);
            String greenCashToString = Double.toString(greenCash);

            cbEdt.setText(cbToString);
            trEdt.setText(trToString);
            greenCashEdt.setText(greenCashToString);

            description = getArguments().getString("description");
            descriptionEdt.setText(description);

            createAt = getArguments().getString("createAt");
            dateTxt.setText(createAt);
        }


        return root;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_menu_save_quit, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_quit:
                AlertDialog.Builder build = new AlertDialog.Builder(getContext());

                build.setMessage("Annuler la modification ?")
                        .setPositiveButton(R.string.yes, (dialog, idExpense) -> {
                            requireActivity().onBackPressed();


                        })
                        .setNegativeButton(R.string.no, (dialog, id) -> {
                        });
                build.show();
                break;
            case R.id.action_save:


                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                builder.setMessage("Sauvegarder les modifications?")
                        .setPositiveButton(R.string.yes, (dialog, idExpense) -> {
                            Bundle bundle = new Bundle();
                            bundle.putString("title", titleEdt.getText().toString());
                            bundle.putString("description", descriptionEdt.getText().toString());
                            bundle.putDouble("tr", Double.parseDouble(trEdt.getText().toString()));
                            bundle.putDouble("cb", Double.parseDouble(cbEdt.getText().toString()));
                            bundle.putDouble("greenCash", Double.parseDouble(greenCashEdt.getText().toString()));
                            bundle.putString("createAt", createAt);
                            bundle.putString("id", id);

                            IncomeDTO incomeDTO = new IncomeDTO();
                            incomeDTO.setId(id);
                            incomeDTO.setiDescription(descriptionEdt.getText().toString());
                            incomeDTO.setiCreditCard(Double.parseDouble(cbEdt.getText().toString()));
                            incomeDTO.setiGreenCash(Double.parseDouble(greenCashEdt.getText().toString()));
                            incomeDTO.setiTr(Double.parseDouble(trEdt.getText().toString()));
                            incomeDTO.setiTitle(titleEdt.getText().toString());


                            NetworkProvider.getInstance().updateIncomeById(incomeDTO);


                            DetailIncomeFragment detailsIncomeFragment = new DetailIncomeFragment();
                            detailsIncomeFragment.setArguments(bundle);

                            FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            //fragmentManager.popBackStack();
                            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                            fragmentTransaction.replace(R.id.nav_host_fragment, detailsIncomeFragment);
                            fragmentTransaction.commit();

                        })
                        .setNegativeButton(R.string.no, (dialog, id) -> {
                        });
                builder.show();


                break;
        }

        return super.onOptionsItemSelected(item);
    }
}