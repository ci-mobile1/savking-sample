package com.example.savking_sample.fragment.profil;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;


import com.example.savking_sample.R;

import java.util.Objects;

public class ProfilFragment extends Fragment {



    private ImageView category;
    private ImageView economy;
    private ImageView preference;
    private ImageView myAccount;
    private ImageView myProgram;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_profil, container, false);
        Objects.requireNonNull(((AppCompatActivity) requireActivity()).getSupportActionBar()).setTitle("Sav[K]ing ");

        category = root.findViewById(R.id.profil_category);
        economy = root.findViewById(R.id.profil_wallet_months_passed);
        preference = root.findViewById(R.id.profil_preferences);
        myAccount = root.findViewById(R.id.profil_my_account);
        myProgram = root.findViewById(R.id.profil_my_expense_programmed);

        category.setOnClickListener(v -> {

        });

        economy.setOnClickListener(v-> {

        });

        preference.setOnClickListener(v-> {
            AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
            alertDialog.setTitle("Information");
            alertDialog.setMessage("\"Mes préférences\" permettra de configurer l'application à votre guise.\n" +
                    "Ces réglages seront accessible lors de l'une des prochaines mises à jour de l'application.");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Fermer",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            Toast toast = Toast.makeText(getContext(), "En cours de développement" , Toast.LENGTH_LONG);
            toast.show();
        });

        myAccount.setOnClickListener(v-> {

        });
        myProgram.setOnClickListener(v-> {

        });

        return root;
    }



}