package com.example.savking_sample.fragment.cashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.example.savking_sample.R;
import com.example.savking_sample.fragment.cashboard.expense.AddExpenseFragment;
import com.example.savking_sample.fragment.cashboard.expense.CashFragment;
import com.example.savking_sample.fragment.cashboard.income.IncomeFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.Date;
import java.util.Objects;

public class CashboardFragment extends Fragment {
    ViewPager viewPager;
    TabLayout tabLayout;
    TextView infoWallet;
    private String month = "";
    private CashboardViewModel cashboardViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        cashboardViewModel =
                ViewModelProviders.of(this).get(CashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_cashboard, container, false);
        Objects.requireNonNull(((AppCompatActivity) requireActivity()).getSupportActionBar()).setTitle("Sav[K]ing");

        viewPager = root.findViewById(R.id.cash_view_pager);
        infoWallet = root.findViewById(R.id.cashboard_wallet);
        tabLayout = root.findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("Dépenses"));
        tabLayout.addTab(tabLayout.newTab().setText("Revenu"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        monthActual();

        return root;


    }

    public static CashboardFragment newInstance() {
        CashboardFragment fragment = new CashboardFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setUpViewPager(viewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if(tab.getPosition() == 1) {
                    infoWallet.setText("Revenu(s) - " + month);
                } else {
                    infoWallet.setText("Dépense(s) - " + month);

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    private void setUpViewPager(ViewPager viewPager) {
        com.example.savking_sample.fragment.cashboard.CashPageAdapter adapter = new com.example.savking_sample.fragment.cashboard.CashPageAdapter(getChildFragmentManager());
        adapter.addFragment(new CashFragment(), "Dépenses");
        adapter.addFragment(new IncomeFragment(), "Revenus");
        viewPager.setAdapter(adapter);
    }

    private void monthActual () {
        int monthNumber = new Date().getMonth();
        monthNumber++;

        switch (monthNumber) {
            case 1:
                month = "Janvier";
                break;
            case 2:
                month = "Février";
                break;
            case 3:
                month = "Mars";
                break;
            case 4:
                month = "Avril";
                break;
            case 5:
                month = "Mai";
                break;
            case 6:
                month = "Juin";
                break;
            case 7:
                month = "Juillet";
                break;
            case 8:
                month = "Août";
                break;
            case 9:
                month = "Septembre";
                break;
            case 10:
                month = "Octobre";
                break;
            case 11:
                month = "Novembre";
                break;
            case 12:
                month = "Décembre";
                break;
            default:
                month = "Mois Actuel";
        }
        infoWallet.setText("Dépenses - " + month);

    }


}