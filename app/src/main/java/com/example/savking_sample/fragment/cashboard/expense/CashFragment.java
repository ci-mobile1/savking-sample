package com.example.savking_sample.fragment.cashboard.expense;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.savking_sample.FragmentControl;
import com.example.savking_sample.Helpers;
import com.example.savking_sample.models.CashModel;
import com.example.savking_sample.R;
import com.example.savking_sample.services.NetworkProvider;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;


public class CashFragment extends Fragment {


    private RecyclerView recyclerViewCash;
    private com.example.savking_sample.fragment.cashboard.expense.RecyclerViewAdapter recyclerCashsAdapter;

    private String token;
    private String idUser;


    List<CashModel> cashModels;


    FloatingActionButton floatingActionButton;

    public CashFragment() {
        // Required empty public constructor
    }

    public static com.example.savking_sample.fragment.cashboard.expense.CashFragment newInstance() {
        com.example.savking_sample.fragment.cashboard.expense.CashFragment fragment = new com.example.savking_sample.fragment.cashboard.expense.CashFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_cash, container, false);

        floatingActionButton = root.findViewById(R.id.cash_fragment_floating_button);
        recyclerViewCash = root.findViewById(R.id.cash_fragment_recycler_view);
        recyclerViewCash.setLayoutManager(new LinearLayoutManager(getActivity()));

        initRecyclerView();
        loadDataActualMonth();

        SharedPreferences preferences = requireActivity().getSharedPreferences("TOKEN_ID", 0);
        idUser =preferences.getString("idUser", "id");


        floatingActionButton.setOnClickListener( v -> {
            Bundle bundle = new Bundle();
            bundle.putString("idUser", idUser);


            FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();

            FragmentControl.fragmentTransaction(AddExpenseFragment.newInstance(idUser), fragmentManager, bundle, "add_expense");
        });

        return root;
    }

    private void initRecyclerView() {

        recyclerViewCash.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerCashsAdapter = new com.example.savking_sample.fragment.cashboard.expense.RecyclerViewAdapter(0);
        recyclerViewCash.setAdapter(recyclerCashsAdapter);

        recyclerCashsAdapter.setItemClickListener(new com.example.savking_sample.fragment.cashboard.expense.RecyclerViewAdapter.ItemClickListener() {
            @Override
            public void onClick(CashModel cashModel) {

                Helpers helpers = new Helpers();
                String createAt = helpers.dateToString(cashModel.getDate());

                Bundle bundle = new Bundle();

                bundle.putString("title", cashModel.getTitle());
                bundle.putString("description", cashModel.getDescription());
                bundle.putString("createAt", createAt);
                bundle.putString("category", cashModel.getCategory());
                bundle.putString("id", cashModel.getId());
                bundle.putDouble("cb", cashModel.getCreditCard());
                bundle.putDouble("tr", cashModel.getcTR());
                bundle.putDouble("greenCash", cashModel.getGreenCash());



                com.example.savking_sample.fragment.cashboard.expense.DetailCashFragment detailCashFragment = new com.example.savking_sample.fragment.cashboard.expense.DetailCashFragment();
                FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                FragmentControl.fragmentTransaction(detailCashFragment, fragmentManager, bundle, "detail_expense");
            }
        });

        recyclerCashsAdapter.setButtonClickListener((cashModel, position) -> {
            boolean isCashSelected = recyclerCashsAdapter.cashList.get(position).isSelected();
            recyclerCashsAdapter.cashList.get(position).setSelected(!isCashSelected);
            //recyclerCashsAdapter.dataFull.get(position).setSelected(true);

            recyclerCashsAdapter.notifyDataSetChanged();

        });

    }

    private void loadDataActualMonth() {
        SharedPreferences preferences = requireActivity().getSharedPreferences("TOKEN_ID", 0);
        token = preferences.getString("token", "token");
        String idUser = preferences.getString("idUser", "id");

        NetworkProvider.getInstance().getCashByActualMonth(token, idUser, new NetworkProvider.Listener<CashModel>() {
            @Override
            public void onSuccess(List<CashModel> data) {
                if(data != null){
                    cashModels = data;
                    recyclerCashsAdapter.setDataCash(data);
                }
            }

            @Override
            public void onError(Throwable t) {
                Log.d("cashData", t.toString());
            }
        });

    }

}