package com.example.savking_sample.fragment.cashboard.income;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.savking_sample.FragmentControl;
import com.example.savking_sample.Helpers;
import com.example.savking_sample.models.IncomeModel;
import com.example.savking_sample.R;
import com.example.savking_sample.services.NetworkProvider;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;


public class IncomeFragment extends Fragment {


    private RecyclerView recyclerViewCash;
    private IncomeRecyclerViewAdapter recyclerCashsAdapter;
    FloatingActionButton floatingActionButton;
    private String token;

    public IncomeFragment() {
        // Required empty public constructor
    }


    public static IncomeFragment newInstance(String param1, String param2) {
        IncomeFragment fragment = new IncomeFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root= inflater.inflate(R.layout.fragment_income, container, false);
        floatingActionButton = root.findViewById(R.id.income_fragment_floating_button);

        recyclerViewCash = root.findViewById(R.id.income_fragment_recycler_view);
        recyclerViewCash.setLayoutManager(new LinearLayoutManager(getActivity()));
        initRecyclerView();
        loadDataActualMonth();


        floatingActionButton.setOnClickListener( v -> {

            SharedPreferences preferences = requireActivity().getSharedPreferences("TOKEN_ID", 0);
            String idUser = preferences.getString("idUser", "id");
            Bundle bundle = new Bundle();
            bundle.putString("idUser", idUser);

            AddIncomeFragment addIncomeFragment = new AddIncomeFragment();
            addIncomeFragment.setArguments(bundle);



            FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.replace(R.id.nav_host_fragment, addIncomeFragment);
            fragmentTransaction.addToBackStack("addIncome").commit();
        });
        return root;

    }

    private void loadDataActualMonth() {
        SharedPreferences preferences = requireActivity().getSharedPreferences("TOKEN_ID", 0);
        token = preferences.getString("token", "token");
        String idUser = preferences.getString("idUser", "id");

        NetworkProvider.getInstance().getIncomeByActualMonth(token, idUser, new NetworkProvider.ListenerIncome<IncomeModel>() {
            @Override
            public void onSuccess(List<IncomeModel> data) {
                if(data != null){
                    recyclerCashsAdapter.setDataIncome(data);

                }
            }

            @Override
            public void onError(Throwable t) {
                Log.d("cashData", t.toString());
            }
        });
    }

    private void initRecyclerView() {

        recyclerViewCash.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerCashsAdapter = new IncomeRecyclerViewAdapter(0);
        recyclerViewCash.setAdapter(recyclerCashsAdapter);

        recyclerCashsAdapter.setItemClickListener(incomeModel -> {
            Helpers helpers = new Helpers();
            String createAt = helpers.dateToString(incomeModel.getDate());
            Bundle bundle = new Bundle();
            bundle.putString("title", incomeModel.getiTitle());
            bundle.putString("description", incomeModel.getiDescription());
            bundle.putString("createAt", createAt);
            bundle.putString("id", incomeModel.getId());
            bundle.putDouble("cb", incomeModel.getiCreditCard());
            bundle.putDouble("tr", incomeModel.getiTr());
            bundle.putDouble("greenCash", incomeModel.getiGreenCash());
            bundle.putString("category",incomeModel.getCategory());


            FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
            FragmentControl.fragmentTransaction(DetailIncomeFragment.newInstance(),fragmentManager, bundle,"detail_income");


        });

    }

}