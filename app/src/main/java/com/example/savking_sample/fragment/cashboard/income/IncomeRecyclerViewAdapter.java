package com.example.savking_sample.fragment.cashboard.income;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;


import com.example.savking_sample.Helpers;
import com.example.savking_sample.models.IncomeModel;
import com.example.savking_sample.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class IncomeRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private List<IncomeModel> incomeList;
    private List<IncomeModel> dataFull = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private int countChar = 0;

    public void setDataIncome(List<IncomeModel> incomeModelList) {
        if(incomeModelList != null){
            this.incomeList = incomeModelList;
            this.dataFull.addAll(incomeModelList);
            notifyDataSetChanged();
        }

    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        if(viewType == 0) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_recycler_row, viewGroup, false);
            return new IncomeViewHolder(view);
        }


        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_expense_block_view_holder, viewGroup, false);
        return new IncomeBlockViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        double total = 0.0;
        final IncomeModel income = incomeList.get(position);

        if(type == 0) {
            IncomeViewHolder incomeViewHolder = (IncomeViewHolder) holder;
            incomeViewHolder.recycler_title.setText(income.getiTitle());
            incomeViewHolder.recycler_category.setText(income.getCategory());

            //if (!Objects.equals(cash.getCreditCard(), "")) {
            double convertionAmount = income.getiCreditCard();
            total+=convertionAmount;
            //}
            //if (!Objects.equals(cash.getGreenCash(), "")) {
            double esp = income.getiGreenCash();
            total+=esp;
            //}
            //if (!Objects.equals(cash.getcTR(), "")) {
            double tr = income.getiTr();
            total+=tr;
            //}

            NumberFormat nf = new DecimalFormat("0.##");
            nf.setMinimumFractionDigits(2);
            String valueAmount = nf.format(total);


            ((IncomeViewHolder) holder).recycler_amount.setText(valueAmount);

            if (itemClickListener != null) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemClickListener.onClick(income);
                    }
                });

            }

        } else {

            NumberFormat nf = new DecimalFormat("0.##");
            nf.setMinimumFractionDigits(2);
            String valueAmount = nf.format(income.getiCreditCard());

            IncomeBlockViewHolder incomeBlockViewHolder = (IncomeBlockViewHolder) holder;
            incomeBlockViewHolder.title_block.setText(income.getiTitle());
            incomeBlockViewHolder.amount_block.setText(valueAmount + " €");
            incomeBlockViewHolder.category.setText(income.getCategory());

            if(income.getDate() != null) {
                Date creationDate  = income.getDate();
                Helpers helpers = new Helpers();
                String createString = helpers.dateLocalFrance(creationDate);
                ((IncomeBlockViewHolder) holder).date_block.setText(createString);
            } else {
                ((IncomeBlockViewHolder) holder).category.setText(R.string.not_available);
            }

        }

        //Glide.with(weaponViewHolder.itemView).load(weapon.getPictureUrl()).into(weaponViewHolder.pictureImv);

    }




    @Override
    public int getItemCount() {
        return incomeList != null ? incomeList.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return filterResearch;
    }


    private Filter filterResearch = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<IncomeModel> filteredList = new ArrayList<>();
            if(constraint == null|| constraint.length() == 0) {
                filteredList.addAll(dataFull);
            }else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                if(filterPattern.length() != 0){
                    incomeList.clear();
                    incomeList.addAll(dataFull);
                }
                for(IncomeModel income : incomeList){
                    if(income.getiTitle().toLowerCase().trim().contains(filterPattern)){
                        filteredList.add(income);
                    }
                }
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //if(cashList != null){
            incomeList.clear();
            incomeList.addAll((List<IncomeModel>) results.values);
            notifyDataSetChanged();
            //}

        }
    };



    class IncomeViewHolder extends RecyclerView.ViewHolder  {
        ImageView recycler_logo_row;
        TextView recycler_title;
        TextView recycler_amount;
        TextView recycler_category;
        ConstraintLayout parent_layout;

        IncomeViewHolder(View itemView) {
            super(itemView);
            this.itemView.findViewById(itemView.getId());

            recycler_title = itemView.findViewById(R.id.recycler_expense_name);
            recycler_amount = itemView.findViewById(R.id.recycler_amount);
            recycler_category = itemView.findViewById(R.id.recycler_category_name);
            parent_layout = itemView.findViewById(R.id.parent_layout_recycler);
            recycler_logo_row = itemView.findViewById(R.id.recycler_logo_row);

        }


    }

    class IncomeBlockViewHolder extends RecyclerView.ViewHolder  {
        ImageView recycler_logo_row;
        TextView date_block;
        TextView title_block;
        TextView amount_block;
        TextView category;


        IncomeBlockViewHolder(View itemView) {
            super(itemView);
            this.itemView.findViewById(itemView.getId());

            title_block = itemView.findViewById(R.id.block_title_txt);
            date_block = itemView.findViewById(R.id.block_date_txt);
            amount_block = itemView.findViewById(R.id.block_amount_txt);
            category = itemView.findViewById(R.id.block_category_txt);

        }


    }

    public int getItemViewType(int position) {
        if(type == 1 ) {
            return 1;
        }
        return 0;
    }

    int type;

    public IncomeRecyclerViewAdapter (int typ) {
        type = typ;
    }


    public interface ItemClickListener {
        void onClick(IncomeModel incomeModel);
    }


}
