package com.example.savking_sample.fragment.cashboard.income;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import com.example.savking_sample.DTO.IncomeDTO;
import com.example.savking_sample.FragmentControl;
import com.example.savking_sample.models.CategoryModel;
import com.example.savking_sample.R;
import com.example.savking_sample.fragment.cashboard.CashboardFragment;
import com.example.savking_sample.services.NetworkProvider;

import java.util.ArrayList;
import java.util.List;

public class AddIncomeFragment extends Fragment {


    EditText titlEdt;
    EditText descriptionEdt;
    EditText cbEdt;
    EditText trEdt;
    EditText espEdt;
    Button sendBtn;

    Spinner sCategory;

    ArrayAdapter<String> adapterIncomeCategory;
    ArrayAdapter<String> defaultAdapterCategory;

    String token;
    String idUser;
    String defaultIdCategoryList = "5f29230a7418b800043066df";

    List<CategoryModel> categoryModelList = new ArrayList<>();


    List<String> defaultCategoryNameList = new ArrayList<>();



    public AddIncomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_income, container, false);
        titlEdt = root.findViewById(R.id.income_add_title);
        descriptionEdt = root.findViewById(R.id.income_add_description);
        cbEdt = root.findViewById(R.id.income_add_cb);
        trEdt = root.findViewById(R.id.income_add_tr);
        espEdt = root.findViewById(R.id.income_add_green_cash);
        sendBtn = root.findViewById(R.id.income_add_btn);
        sCategory = root.findViewById(R.id.income_spinnerCategory);

        SharedPreferences preferences = requireActivity().getSharedPreferences("TOKEN_ID", 0);
        token = preferences.getString("token", "token");
        idUser = preferences.getString("idUser", "id");


        String selectACategory = "Séléctionnez une catégorie";
        defaultCategoryNameList.add(selectACategory);

        defaultAdapterCategory = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_item, defaultCategoryNameList);
        defaultAdapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sCategory.setAdapter(defaultAdapterCategory);

        

        loadCategories(defaultIdCategoryList);
        loadCategories(idUser);

        sendBtn.setOnClickListener(s-> {
            String categoryChosen = sCategory.getSelectedItem().toString();

            if(titlEdt.getText().toString().trim().matches("")) {
                Toast toast = Toast.makeText(getContext(), "Titre manquant", Toast.LENGTH_SHORT);
                toast.show();
            }else {

                IncomeDTO incomeDTO = new IncomeDTO();

                if(!cbEdt.getText().toString().trim().matches("")){
                    incomeDTO.setiCreditCard(Double.parseDouble(cbEdt.getText().toString()));
                }
                if(!trEdt.getText().toString().trim().matches("")){
                    incomeDTO.setiTr(Double.parseDouble(trEdt.getText().toString()));
                }
                if(!espEdt.getText().toString().trim().matches("")){
                    incomeDTO.setiGreenCash(Double.parseDouble(espEdt.getText().toString()));
                }

                incomeDTO.setiTitle(titlEdt.getText().toString());

                if(!descriptionEdt.getText().toString().trim().matches("")){
                    incomeDTO.setiDescription(descriptionEdt.getText().toString());
                }

                String idCategory = getIdCategorySelected(categoryChosen);

                incomeDTO.setCategory(idCategory);
                incomeDTO.setiIdUser(idUser);

                NetworkProvider.getInstance().addNewIncome(incomeDTO);

                Toast toast = Toast.makeText(getContext(), "Revenu ajouté!", Toast.LENGTH_LONG);
                toast.show();


                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentControl.fragmentTransaction(CashboardFragment.newInstance(),fragmentManager, null,null);
            }
        });
        return root;
    }

    private void loadCategories (String idUserCategoryOwner) {

        NetworkProvider.getInstance().getCategoriesByUser(idUserCategoryOwner, token, 1,new NetworkProvider.ListenerCategory<CategoryModel>() {
            @Override
            public void onSuccess(List<CategoryModel> data) {
                if(data != null ) {
                    categoryModelList.addAll(data);
                }

                for(CategoryModel categoryModel : data) {
                    defaultCategoryNameList.add(categoryModel.getCategoryName());
                }
                if(idUserCategoryOwner.matches(idUser)) {

                    adapterIncomeCategory = new ArrayAdapter<>(requireContext(),
                            android.R.layout.simple_spinner_item, defaultCategoryNameList);
                    adapterIncomeCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    sCategory.setAdapter(adapterIncomeCategory);

                }
            }

            @Override
            public void onError(Throwable t) {

            }
        });
    }
    private String getIdCategorySelected(String categorySelected) {
        for(CategoryModel categoryModel : categoryModelList) {
            if(categoryModel.getCategoryName().matches(categorySelected)) {
                return categoryModel.getId();
            }
        }
        return "";
    }
}