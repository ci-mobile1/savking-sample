package com.example.savking_sample;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.savking_sample.fragment.cashboard.CashboardFragment;
import com.example.savking_sample.fragment.home.HomeFragment;
import com.example.savking_sample.fragment.profil.ProfilFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Sav[K]ing");
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorToolbar));
        setSupportActionBar(toolbar);


        Fragment selectedFragment = new HomeFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                Objects.requireNonNull(selectedFragment)).commit();
        navView.setOnNavigationItemSelectedListener(navListener);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            item -> {
                Fragment selectedFragment = null;

                switch (item.getItemId()) {
                    case R.id.navigation_cashboard:
                        selectedFragment = new CashboardFragment();
                        break;
                    case R.id.navigation_home:
                        selectedFragment = new HomeFragment();
                        break;
                    case R.id.navigation_profil:
                        selectedFragment = new ProfilFragment();
                        break;
                }

                MainActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                        Objects.requireNonNull(selectedFragment)).addToBackStack("profil").commit();
                return true;
            };

    @Override
    public void onBackPressed() {

        if(getSupportFragmentManager().getBackStackEntryCount() >1) {
            getSupportFragmentManager().popBackStack();
        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setMessage("Se déconnecter ?")
                    .setPositiveButton(R.string.yes, (dialog, id) -> {
                        SharedPreferences.Editor editor = getSharedPreferences("STAY_CONNECTED", MODE_PRIVATE).edit();
                        editor.putString("connected", "false");
                        editor.apply();
                        SharedPreferences.Editor pref = getSharedPreferences("TOKEN_ID", MODE_PRIVATE).edit();
                        pref.putString("token", "false");
                        pref.putString("idUser", "false");
                        pref.apply();
                        Intent intent = new Intent(this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        //this.finish();
                    })
                    .setNegativeButton(R.string.no, (dialog, id) -> {
                    });
            builder.show();
        }


    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        if(menu.findItem(10) == null){
            MenuItem item = menu.add(Menu.NONE,10,5,"Déconnexion");
            item.setIcon(R.drawable.ic_baseline_power_settings_new_24);
            item.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
            item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {

                    alertDeconnexion();
                    return false;
                }
            });
        }

        //menu.removeItem(3);
        return super.onPrepareOptionsMenu(menu);
    }

    public void alertDeconnexion() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Se déconnecter ?")
                .setPositiveButton(R.string.yes, (dialog, id) -> {
                    SharedPreferences.Editor editor = getSharedPreferences("STAY_CONNECTED", MODE_PRIVATE).edit();
                    editor.putString("connected", "false");
                    editor.apply();
                    SharedPreferences.Editor pref = getSharedPreferences("TOKEN_ID", MODE_PRIVATE).edit();
                    pref.putString("token", "false");
                    pref.putString("idUser", "false");
                    pref.apply();
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    //this.finish();
                })
                .setNegativeButton(R.string.no, (dialog, id) -> {
                });
        builder.show();
    }
}