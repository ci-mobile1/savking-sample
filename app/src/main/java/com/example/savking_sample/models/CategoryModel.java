package com.example.savking_sample.models;

public class CategoryModel {

    private String idUser;
    private String id;
    private int categoryType;
    private String categoryName;
    public boolean selectedInList;
    public boolean selectedToBeEdit;
    public boolean selectedToBeDelete;

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(int categoryType) {
        this.categoryType = categoryType;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public boolean isSelectedInList() {
        return selectedInList;
    }

    public void setSelectedInList(boolean selectedInList) {
        this.selectedInList = selectedInList;
    }

    public boolean isSelectedToBeEdit() {
        return selectedToBeEdit;
    }

    public void setSelectedToBeEdit(boolean selectedToBeEdit) {
        this.selectedToBeEdit = selectedToBeEdit;
    }

    public boolean isSelectedToBeDelete() {
        return selectedToBeDelete;
    }

    public void setSelectedToBeDelete(boolean selectedToBeDelete) {
        this.selectedToBeDelete = selectedToBeDelete;
    }

    @Override
    public String toString() {
        return "CategoryModel{" +
                "idUser='" + idUser + '\'' +
                ", id='" + id + '\'' +
                ", categoryType=" + categoryType +
                ", categoryName='" + categoryName + '\'' +
                ", selectedInList=" + selectedInList +
                ", selectedToBeEdit=" + selectedToBeEdit +
                ", selectedToBeDelete=" + selectedToBeDelete +
                '}';
    }
}
