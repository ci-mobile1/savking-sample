package com.example.savking_sample.models;

import java.util.Date;

public class IncomeModel {

    private double iCreditCard;
    private double iGreenCash;
    private double iTr;
    private String iDescription;
    private String iTitle;
    private String iIdUser;
    private Date date;
    private String id;
    private String Category;

    public double getiCreditCard() {
        return iCreditCard;
    }

    public void setiCreditCard(double iCreditCard) {
        this.iCreditCard = iCreditCard;
    }

    public double getiGreenCash() {
        return iGreenCash;
    }

    public void setiGreenCash(double iGreenCash) {
        this.iGreenCash = iGreenCash;
    }

    public double getiTr() {
        return iTr;
    }

    public void setiTr(double iTr) {
        this.iTr = iTr;
    }

    public String getiDescription() {
        return iDescription;
    }

    public void setiDescription(String iDescription) {
        this.iDescription = iDescription;
    }

    public String getiTitle() {
        return iTitle;
    }

    public void setiTitle(String iTitle) {
        this.iTitle = iTitle;
    }

    public String getiIdUser() {
        return iIdUser;
    }

    public void setiIdUser(String iIdUser) {
        this.iIdUser = iIdUser;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    @Override
    public String toString() {
        return "IncomeModel{" +
                "iCreditCard=" + iCreditCard +
                ", iGreenCash=" + iGreenCash +
                ", iTr=" + iTr +
                ", iDescription='" + iDescription + '\'' +
                ", iTitle='" + iTitle + '\'' +
                ", iIdUser='" + iIdUser + '\'' +
                ", date=" + date +
                ", id='" + id + '\'' +
                ", Category='" + Category + '\'' +
                '}';
    }
}