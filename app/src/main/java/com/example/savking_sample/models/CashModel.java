package com.example.savking_sample.models;

import java.util.Date;

public class CashModel  {
    private double greenCash;
    private String category;
    private String title;
    private double creditCard;
    private Date date;
    private String description;
    private double cTR;
    private String id;
    private String idUser;
    boolean isSelected;

    public double getGreenCash() {
        return greenCash;
    }

    public void setGreenCash(double greenCash) {
        this.greenCash = greenCash;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(double creditCard) {
        this.creditCard = creditCard;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getcTR() {
        return cTR;
    }

    public void setcTR(double cTR) {
        this.cTR = cTR;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public String toString() {
        return "CashModel{" +
                "greenCash=" + greenCash +
                ", category='" + category + '\'' +
                ", title='" + title + '\'' +
                ", creditCard=" + creditCard +
                ", date=" + date +
                ", description='" + description + '\'' +
                ", cTR=" + cTR +
                ", id='" + id + '\'' +
                ", idUser='" + idUser + '\'' +
                ", isSelected=" + isSelected +
                '}';
    }
}
