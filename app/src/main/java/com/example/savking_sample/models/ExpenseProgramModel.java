package com.example.savking_sample.models;

import java.util.Date;

public class ExpenseProgramModel {

    private double eGreenCash;
    private double eCreditCard;
    private double eTR;
    private String eDescription;
    private String eCategory;
    private String eTitle;
    private String eIdUser;
    private Date eDateBegin;
    private Date eDateEnd;
    private boolean eSwitch;
    private Date eLastTimeAdded;
    private Date eTypeOfProgram;
    private String _id;

    public double geteGreenCash() {
        return eGreenCash;
    }

    public void seteGreenCash(double eGreenCash) {
        this.eGreenCash = eGreenCash;
    }

    public double geteCreditCard() {
        return eCreditCard;
    }

    public void seteCreditCard(double eCreditCard) {
        this.eCreditCard = eCreditCard;
    }

    public double geteTR() {
        return eTR;
    }

    public void seteTR(double eTR) {
        this.eTR = eTR;
    }

    public String geteDescription() {
        return eDescription;
    }

    public void seteDescription(String eDescription) {
        this.eDescription = eDescription;
    }

    public String geteCategory() {
        return eCategory;
    }

    public void seteCategory(String eCategory) {
        this.eCategory = eCategory;
    }

    public String geteTitle() {
        return eTitle;
    }

    public void seteTitle(String eTitle) {
        this.eTitle = eTitle;
    }

    public String geteIdUser() {
        return eIdUser;
    }

    public void seteIdUser(String eIdUser) {
        this.eIdUser = eIdUser;
    }

    public Date geteDateBegin() {
        return eDateBegin;
    }

    public void seteDateBegin(Date eDateBegin) {
        this.eDateBegin = eDateBegin;
    }

    public boolean iseSwitch() {
        return eSwitch;
    }

    public void seteSwitch(boolean eSwitch) {
        this.eSwitch = eSwitch;
    }

    public Date geteLastTimeAdded() {
        return eLastTimeAdded;
    }

    public void seteLastTimeAdded(Date eLastTimeAdded) {
        this.eLastTimeAdded = eLastTimeAdded;
    }

    public Date geteTypeOfProgram() {
        return eTypeOfProgram;
    }

    public void seteTypeOfProgram(Date eTypeOfProgram) {
        this.eTypeOfProgram = eTypeOfProgram;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Date geteDateEnd() {
        return eDateEnd;
    }

    public void seteDateEnd(Date eDateEnd) {
        this.eDateEnd = eDateEnd;
    }

    @Override
    public String toString() {
        return "ExpenseProgramModel{" +
                "eGreenCash=" + eGreenCash +
                ", eCreditCard=" + eCreditCard +
                ", eTR=" + eTR +
                ", eDescription='" + eDescription + '\'' +
                ", eCategory='" + eCategory + '\'' +
                ", eTitle='" + eTitle + '\'' +
                ", eIdUser='" + eIdUser + '\'' +
                ", eDateBegin=" + eDateBegin +
                ", eDateEnd=" + eDateEnd +
                ", eSwitch=" + eSwitch +
                ", eLastTimeAdded=" + eLastTimeAdded +
                ", eTypeOfProgram=" + eTypeOfProgram +
                ", _id='" + _id + '\'' +
                '}';
    }
}
