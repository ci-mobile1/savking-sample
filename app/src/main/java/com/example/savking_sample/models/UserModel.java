package com.example.savking_sample.models;


import java.util.Date;

public class UserModel {
    private String id;
    private String email;
    private String uAccessType;
    private String uPassword;
    private String uPseudo;
    private Date uCreationDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getuAccessType() {
        return uAccessType;
    }

    public void setuAccessType(String uAccessType) {
        this.uAccessType = uAccessType;
    }

    public String getuPassword() {
        return uPassword;
    }

    public void setuPassword(String uPassword) {
        this.uPassword = uPassword;
    }

    public String getuPseudo() {
        return uPseudo;
    }

    public void setuPseudo(String uPseudo) {
        this.uPseudo = uPseudo;
    }

    public Date getuCreationDate() {
        return uCreationDate;
    }

    public void setuCreationDate(Date uCreationDate) {
        this.uCreationDate = uCreationDate;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", uAccessType='" + uAccessType + '\'' +
                ", uPassword='" + uPassword + '\'' +
                ", uPseudo='" + uPseudo + '\'' +
                ", uCreationDate=" + uCreationDate +
                '}';
    }
}
