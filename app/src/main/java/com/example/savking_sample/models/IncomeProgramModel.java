package com.example.savking_sample.models;

import java.util.Date;

public class IncomeProgramModel {

    private double iGreenCash;
    private double iCreditCard;
    private double iTR;
    private String iDescription;
    private String iCategory;
    private String iTitle;
    private String iIdUser;
    private Date iDateBegin;
    private Date iDateEnd;
    private boolean iSwitch;
    private Date iLastTimeAdded;
    private Date iTypeOfProgram;
    private String _id;

    public double getiGreenCash() {
        return iGreenCash;
    }

    public void setiGreenCash(double iGreenCash) {
        this.iGreenCash = iGreenCash;
    }

    public double getiCreditCard() {
        return iCreditCard;
    }

    public void setiCreditCard(double iCreditCard) {
        this.iCreditCard = iCreditCard;
    }

    public double getiTR() {
        return iTR;
    }

    public void setiTR(double iTR) {
        this.iTR = iTR;
    }

    public String getiDescription() {
        return iDescription;
    }

    public void setiDescription(String iDescription) {
        this.iDescription = iDescription;
    }

    public String getiCategory() {
        return iCategory;
    }

    public void setiCategory(String iCategory) {
        this.iCategory = iCategory;
    }

    public String getiTitle() {
        return iTitle;
    }

    public void setiTitle(String iTitle) {
        this.iTitle = iTitle;
    }

    public String getiIdUser() {
        return iIdUser;
    }

    public void setiIdUser(String iIdUser) {
        this.iIdUser = iIdUser;
    }

    public Date getiDateBegin() {
        return iDateBegin;
    }

    public void setiDateBegin(Date iDateBegin) {
        this.iDateBegin = iDateBegin;
    }

    public Date getiDateEnd() {
        return iDateEnd;
    }

    public void setiDateEnd(Date iDateEnd) {
        this.iDateEnd = iDateEnd;
    }

    public boolean isiSwitch() {
        return iSwitch;
    }

    public void setiSwitch(boolean iSwitch) {
        this.iSwitch = iSwitch;
    }

    public Date getiLastTimeAdded() {
        return iLastTimeAdded;
    }

    public void setiLastTimeAdded(Date iLastTimeAdded) {
        this.iLastTimeAdded = iLastTimeAdded;
    }

    public Date getiTypeOfProgram() {
        return iTypeOfProgram;
    }

    public void setiTypeOfProgram(Date iTypeOfProgram) {
        this.iTypeOfProgram = iTypeOfProgram;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    @Override
    public String toString() {
        return "IncomeProgramModel{" +
                "iGreenCash=" + iGreenCash +
                ", iCreditCard=" + iCreditCard +
                ", iTR=" + iTR +
                ", iDescription='" + iDescription + '\'' +
                ", iCategory='" + iCategory + '\'' +
                ", iTitle='" + iTitle + '\'' +
                ", iIdUser='" + iIdUser + '\'' +
                ", iDateBegin=" + iDateBegin +
                ", iDateEnd=" + iDateEnd +
                ", iSwitch=" + iSwitch +
                ", iLastTimeAdded=" + iLastTimeAdded +
                ", iTypeOfProgram=" + iTypeOfProgram +
                ", _id='" + _id + '\'' +
                '}';
    }
}
